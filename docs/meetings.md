* * *
![nswc-logo](img/nswc-logo-long.png)

* * *
## Public Meetings

Regular meetings are usually held on the 2nd Wednesday of the month at 8:00 AM. At these meetings, it is common for the Commission to discuss and take action on issues including water quality, projects, and expenses. The next regular meeting is scheduled for **8:00 AM on Wednesday, April 9, 2025**. Approximately 3 to 5 days prior to the meeting, an agenda and information regarding how to access the meeting will be provided. If you would like more information about the meeting, please call our office at 414-963-0160.

<!-- Please note that the next meeting will be at the Water Filtration Plant located at 400 W Bender Rd, Glendale, WI 53217.
[Click here to get directions.](https://goo.gl/maps/RG9Wj) -->

* * *

*The public is welcome to attend any of our meetings!*

<!-- You are welcome to attend the meeting virtually using the following URL: </br>
[https://us02web.zoom.us/j/83605228177](https://us02web.zoom.us/j/83605228177) and/or call (312) 626-6799 </br>
Meeting ID: 836 0522 8177 | Passcode: 211077 </br>
The meeting will start Wednesday, March 12, 2025 at 8:00 A.M. </br>
Please call (414) 963-0160 to request further assistance. </br> -->

* * *

## Meeting Agenda

*Not available at this time.*
<!-- [:material-file-download: Download Agenda](agenda/NSWC_Agenda_March2025Meeting_03122025.pdf) -->

* * *
## Previous Meetings

Use the sidebar and/or menu to access archives.
