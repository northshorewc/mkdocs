* * *
![nswc-logo](img/nswc-logo-long.png)

* * *
## Public Meetings

Regular meetings are usually held on the 2nd Wednesday of the month at 8:00 AM. At these meetings, it is common for the Commission to discuss and take action on issues including water quality, projects, and expenses. The next regular meeting is scheduled for **8:00 AM on Wednesday, October 9, 2024**. Approximately 3 to 5 days prior to the meeting, an agenda and information regarding how to access the meeting will be provided. If you would like more information about the meeting, please call our office at 414-963-0160.

<!-- Please note that the next meeting will be at the Water Filtration Plant located at 400 W Bender Rd, Glendale, WI 53217.
[Click here to get directions.](https://goo.gl/maps/RG9Wj) -->

* * *

*The public is welcome to attend any of our meetings!*

You are welcome to attend the meeting virtually using the following URL: </br>
[https://us02web.zoom.us/j/82790957199](https://us02web.zoom.us/j/82790957199) and/or call (312) 626-6799 </br>
Meeting ID: 827 9095 7199 | Passcode: 994966 </br>
The meeting will start Wednesday, October 9, 2024 at 8:00 A.M. </br>
Please call (414) 963-0160 to request further assistance. </br>

* * *

## Meeting Agenda

1. Call to order by the Chair.  
2. Discussion of Public Service Commission’s requirement regarding construction authorization and the possibility of retaining SEH and City Water to assist with the application process.
3. Approval of the minutes of the September meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Review of payment reports and approval of payment requests for capital expenditures.
7. Update regarding the progress of capital projects.
8.  Discussion and possible approval to authorize the Plant Manager to sell the existing Klode generator on behalf of the member utilities, with the proceeds going to the Capital Fund.
9. Date and time of the next regular Commission Meeting.
10. Adjournment.


[:material-file-download: Download Agenda](agenda/NSWC_Agenda_October2024Meeting_10092024.pdf)

* * *
## Previous Meetings

Use the sidebar and/or menu to access archives.
