* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

## Lead and Copper Monitoring

In partnership with our member water utilities, we have selected households in Fox Point, Glendale, and Whitefish Bay to participate in our lead and copper monitoring program. Monitoring sites were selected based on stringent criteria determined by the United States Environgmental Protection Agency (USEPA) and the Wisconsin Department of Natural Resources (WDNR). Those households will receive a letter from the Commission describing the monitoring program as well as other information about lead and copper.

[Commission: Letter to Selected Residents](img/letter_to_resident.pdf)

[USEPA: Sources of Lead in Drinking Water](img/epa_lead.pdf)

[North Shore Health Department: FAQs about Lead in Water](img/NSHD_lead.pdf)

[Wisconsin Dept of Natural Resources: Lead in Drinking Water](img/dnr_lead.pdf)

* * *

## Cost

This monitoring is completely **FREE** to participants.

* * *

## Sample Kit

A sample kit will be placed on the doorknob of your front door if you are selected to participate in this monitoring program. Inside the kit, you will find a 1-L bottle, label, marker, and detailed instructions on how to collect the sample. After collecting the sample, simply put everything back in the kit, hang it on your front door, and contact us to pick up the sample.

[Commission: Instructions for Collecting Sample](img/LCR____instructions.pdf)

* * *

## Results

Our staff will complete the necessary paperwork and send water samples to an independent laboratory for analysis. Results from this monitoring effort and information about lead will be provided to residents as soon as practical but no later than 30 days after we learn of the tap monitoring results. If excessive lead and/or copper levels are found, we will notify those particular residents within 1-2 business days after we learn of the results.

* * *

## Contact Information

If you have any questions about this monitoring, lead services, or the health effects of lead, please contact us.

* North Shore Water Commission: 	(414) 963-0160
* North Shore Health Department: 	(414) 371-2980
* Fox Point Water Utility:	      (414) 351-8900
* Glendale Water Utility:       	(414) 228-1719
* Whitefish Bay Water Utility:    (414) 962-6690
