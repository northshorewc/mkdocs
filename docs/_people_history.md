* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

## History of Officials in Office

### Fox Point

Meeting | Commissioner | Alternate      | Notes
--------|--------------|----------------|------
November 2017 | Scott Botcher | Karen Schapiro |
October 2017 | Scott Botcher | Karen Schapiro |
September 2017 | Scott Botcher | Karen Schapiro |
August 2017 | Scott Botcher | Karen Schapiro |
July 2017 | Scott Botcher | vacant |
June 2017 | Scott Botcher | vacant | Mike West Left Office
May 2017 | Mike West | Scott Botcher |
April 2017 | Mike West | Scott Botcher |
March 2017 | Mike West | Scott Botcher |
February 2017 | Mike West | Scott Botcher |
January 2017 | Mike West | Scott Botcher |

### Glendale

Meeting | Commissioner | Alternate     | Notes
--------|--------------|---------------|------
November 2017 | Dave Eastman | Rachel Reiss |
October 2017 | Dave Eastman | Rachel Reiss |
September 2017 | Dave Eastman | Rachel Reiss |
August 2017 | Dave Eastman | Rachel Reiss |
July 2017 | Dave Eastman | Rachel Reiss |
June 2017 | Dave Eastman | Rachel Reiss |
May 2017 | Dave Eastman | Rachel Reiss |
April 2017 | Dave Eastman | Rachel Reiss |
March 2017 | Dave Eastman | Rachel Reiss |
February 2017 | Dave Eastman | Rachel Reiss |
January 2017 | Dave Eastman | Rachel Reiss |

### Whitefish Bay

Meeting | Commissioner | Alternate     | Notes
--------|--------------|---------------|------
November 2017 | John Edlebeck | Paul Boening |
October 2017 | John Edlebeck | Paul Boening |
September 2017 | John Edlebeck | Paul Boening |
August 2017 | John Edlebeck | Paul Boening |
July 2017 | John Edlebeck | Paul Boening |
June 2017 | John Edlebeck | Paul Boening |
May 2017 | John Edlebeck | Paul Boening |
April 2017 | John Edlebeck | Paul Boening |
March 2017 | John Edlebeck | Paul Boening |
February 2017 | John Edlebeck | Paul Boening |
January 2017 | John Edlebeck | Paul Boening |
