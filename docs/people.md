<!-- people.md -->

* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

## Governance

Fox Point, Glendale, and Whitefish Bay jointly own the facilities of the North Shore Water Commission and share the costs of water production and facility improvements.

The NSWC is governed by three appointed Commissioners and three Alternates with equal representation from Fox Point, Glendale, and Whitefish Bay. Each member is appointed by his/her respective municipality for a term of one year. Annually, the responsibilities of Chairman, Secretary and Member are rotated giving each community equal share of authority.

Under normal circumstances, the NSWC convenes monthly and reviews the operation and maintenance of the water treatment facility as well as water quality concerns. After discussion, the NSWC votes on proposed action, spending, and budgets. Capital improvements, such as the UV disinfection upgrade, must be unanimously approved and financed by the member communities.

* * *

## Officials

Please note that officers from the member communities rotate positions annually on May 1.

### Fox Point

Office | Office Holder | Email | Phone
-------|---------------|-------|------
Member | Scott Botcher | [sbotcher@villageoffoxpoint.com](mailto:sbotcher@villageoffoxpoint.com) | (414) 351-8900
Alternate | Scott Brandmeier | [sbrandmeier@villageoffoxpoint.com](mailto:sbrandmeier@villageoffoxpoint.com) | (414) 351-8900
Fiscal Agent | Sara Bruckman | [sbruckman@foxpointwi.gov ](mailto:SBruckman@foxpointwi.gov) | (414) 351-8900


### Glendale

Office | Office Holder | Email | Phone
-------|---------------|-------|------
Chair  | Charlie Imig | [c.imig@glendalewi.gov](mailto:c.imig@glendalewi.gov) | (414) 228-1710
Alternate | Karl Warwick | [k.warwick@glendalewi.gov](mailto:k.warwick@glendalewi.gov) | (414) 228-1714

<!-- Fiscal Agent | Jialin Li | [j.li@glendalewi.gov](mailto:j.li@glendalewi.gov) | (414) 228-1705 -->


### Whitefish Bay

Office       | Office Holder  | Email | Phone
-------------|----------------|-------|------
Secretary    | Matthew Collins  | [m.collins@wfbvillage.gov](mailto:M.Collins@wfbvillage.gov) | (414) 962-6690
Alternate    | Anna Christopherson  | [A.Christopherson@wfbvillage.gov](mailto:a.christopherson@wfbvillage.gov) | (414) 962-6690

<!-- Fiscal Agent | Jaimie Krueger | [j.krueger@wfbvillage.gov](mailto:J.Krueger@wfbvillage.gov) | (414) 962-6690 -->

* * *

## Staff

Eric Kiefer has been the Plant Manager since 2007. He received a Bachelor of Science degree with a major in chemistry from the University of Wisconsin Oshkosh as well as a Master of Business Administration degree from Marquette University. Recently, he has earned A+, Linux+, Network+, and Security+ certifications from CompTIA. All full-time staff that participate in operations, including Mr. Kiefer, have a WDNR Municipal Waterworks Operator Certification. If you have a question regarding your water quality, please contact Mr. Kiefer at [ekiefer@northshorewc.com](mailto:ekiefer@northshorewc.com) or (414) 963-0160.
