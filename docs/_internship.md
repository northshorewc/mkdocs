* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

**Summer: May 27, 2019 to August 23, 2019**<br>

**[Click here if you are interested.](https://tinyurl.com/Summer-2019-Interest-Form)**
* * *

## Internship Description
* * *

Intern will be trained to operate a conventional surface water treatment plant with UV disinfection technology. In addition to operations training, Intern will be assigned additional projects by the Plant Manager. Anticipated projects include printing and applying labels on pipes, performing instrument calibrations, testing water samples, producing short training videos, performing minor concrete rehab projects, and assisting with special lead and copper monitoring.

## North Shore Water Commission Description
* * *

The North Shore Water Commission (NSWC) is a progressive drinking water treatment facility located in the City of Glendale, Wisconsin. It supplies water to its member utilities in Fox Point, Glendale, and Whitefish Bay and also supplies water on a wholesale basis to the City of Mequon.

## Location
* * *

Intern will primarily work at our main office at [400 W Bender Rd, Glendale, WI](https://goo.gl/maps/RG9Wj).

## Conditions
* * *

### Work Schedule

Flexible and can range from 20 to 40 hours per week. It is expected that the Intern works at least 300 hours during a the summer.

The summer internship runs from May 27, 2019 to August 23, 2019.

<!-- Upon mutual agreement, interns may be allowed to extend their employment from one semester to two semesters. For instance, the summer intern may be allowed to stay employed through the end of the fall semester. -->

### Pay

**Hourly rate is $11.48 per hour.**

### Certification Exam

Intern must commit to register and complete the waterworks operator examination (surface water subclass) at the Commission’s expense provided the Wisconsin Department of Natural Resources (WDNR) offers said examination during the internship. A one-time **bonus of $200** will be awarded to the Intern if he/she successfully completes examination, achieves a passing score, and consequently receives valid certification from the State of Wisconsin.

### Other

Internship is limited to students currently enrolled in the Environmental Health and Water Quality Technology program at Milwaukee Area Technical School (MATC), the Water Quality Technology program at Moraine Park Technical College, University of Wisconsin Milwaukee School of Freshwater Sciences, or a related program. Exceptions may be made on a case-by-case basis.

### No Benefits

This position will not receive benefits and will not be eligible for earning shift differentials.

## Tasks and Responsibilities
* * *

### Operations
* Make audio and visual observations of plant equipment.
* Perform laboratory analyses that require fine motor skill.
* Mix and dilute laboratory chemicals and reagents as needed.
* With supervision, make adjustments to equipment using human to machine interface.
* With supervision, turn water treatment equipment on and off.
* Perform janitorial work including sweeping, mopping, and cleaning of fixtures.
* Perform office work including data entry.

### Anticipated Projects
* Gather and analyze data.
* Collect and analyze water samples.
* Use analytical instruments, prepare standards, and other related equipment to calibrate turbidimeters and other online analyzers.
* Print and apply labels to pipes after sufficient surface preparation.
* Create video using mobile device or other audiovisual equipment.
* Edit, annotate, and post video to YouTube channel managed by the Commission.
* Move dirt, prep concrete surface, and make minor repair to concrete.
* Assist plant staff with special lead and copper monitoring. This may include:
    * Driving to remote location and collecting water samples.
    * Performing lab work.
    * Reporting results.

## Knowledge, Skills, Abilities
* * *
* Possess a valid driver's license issued by the State of Wisconsin (class D).
* Operate pickup truck.
* Knowledge of computer systems for typical office use.
* Knowledge of basic chemistry and algebra to perform calculations.
* Knowledge of basic electrical, mechanical, and hydraulic principles.
* Proficiency with internet browsers and searching the Internet.
* Ability to understand and follow oral and written instructions.
* Ability to assemble and process information when necessary.
* Ability to deal with other employees and the general public in a courteous and tactful manner.
* Ability to work in diverse environment.
* Ability to climb steps and ladder.
* Understand and perform basic works skills such as:
    * Arrive to work as scheduled.
    * Dress appropriately.
    * Adhere to imposed deadlines.
    * Use professional and courteous language.

## Supervisor
* * *

Intern will report to Plant Manager. Intern will also take direction from plant staff while performing various tasks.

## Interested?
* * *

Please visit and complete the Interest Form at [https://tinyurl.com/Summer-2019-Interest-Form](https://tinyurl.com/Summer-2019-Interest-Form). If you cannot access the form, please contact Eric Kiefer at info@northshorewc.com or (414) 963-0160. This position may be advertised until filled.

* * *
**Dated 03/22/2019**
