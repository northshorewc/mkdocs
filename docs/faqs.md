* * *
![nswc-logo](img/nswc-logo-long.png)  
* * *

## Where do I send my water bill?

The North Shore Water Commission supplies water to the Village of Fox Point, City of Glendale, Village of Whitefish Bay, and parts of the Mequon Water system. We don't bill residential, commercial, or industrial customers. Each community or wholesale customer is responsible for billing.

* * *

## Can I have my water tested at the North Shore Water Commission?

We can do an industry-standard test for coliform bacteria. This test is universally used to determine if water is safe to drink. Often this test is used for testing private wells. For more information please call (414) 963-0160 or email info@northshorewc.com.

Please note that regulations have changed recently. If you require a water test because you are selling a property, you must use another lab. Please check with the Wisconsin State Lab of Hygiene at (800) 224-6266 for additional information.

* * *

## Who Do I Contact Regarding Lead Testing?

If you are concerned about the presence of lead in your home, please contact the North Shore Health Department at (414) 371-2980 for additional information.

* * *

## What are the benefits of UV disinfection?

Because the technology uses ultraviolet light, no additional chemicals are added for this process.
Pathogens such as cryptosporidium are immediately inactivated as they enter the UV reactor.
Compared to comparable cryptosporidium treatment technologies, UV disinfection is the safest and most cost-effective.
Technology is not affected by water temperature like comparable technologies.

* * *

## How hard is my water ?

Water hardness is a measure of how much calcium and other minerals are dissolved in the water. On average, NSWC water has 137 mg/L of hardness (expressed as mg/L of calcium carbonate). In terms of grains, this is approximately 8 gr/gal. This level of hardness is generally referred to as moderate (on a scale ranging from soft to hard).