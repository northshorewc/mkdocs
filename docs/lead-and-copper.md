All data shown is for the monitoring period starting January 1, 2025 and ending June 30, 2025. <br>
Lasted updated: 3/21/2025 <br>

**Statistics for Lead Results** <br>
*Please note that a value of "0" means that the analyte was not detected.*
<!-- [:material-file-download: Download](pbcu/stats_lead.csv) -->

{{ read_csv('docs/pbcu/stats_lead.csv') }}

**Counts - Lab Results**
<!-- [:material-file-download: Download](pbcu/stats_results.csv) -->

{{ read_csv('docs/pbcu/stats_results.csv') }}

**Counts - Viable Samples Collected**
<!-- [:material-file-download: Download](pbcu/stats_samples.csv) -->

{{ read_csv('docs/pbcu/stats_samples.csv') }}

**Counts - Kits Delivered**
<!-- [:material-file-download: Download](pbcu/stats_kits.csv) -->

{{ read_csv('docs/pbcu/stats_kits.csv') }}

**Lab Results** <br>
*Please note that a value of "0" means that the analyte was not detected.*
[:material-file-download: Download](pbcu/results.csv)

{{ read_csv('docs/pbcu/results.csv') }}

**Valid Samples Collected**
[:material-file-download: Download](pbcu/samples.csv)

{{ read_csv('docs/pbcu/samples.csv') }}

**Sample Kits Delivered**
[:material-file-download: Download](pbcu/kits.csv)

{{ read_csv('docs/pbcu/kits.csv') }}