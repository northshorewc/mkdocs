* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 11, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_January2017Meeting_01112017.pdf)

[Minutes](../../minutes/NSWC_Minutes_February2017Meeting_01112017_signed.pdf)

[Packet](../../materials/NSWC_packet_January2017Meeting_01112017_postmeeting.pdf)
