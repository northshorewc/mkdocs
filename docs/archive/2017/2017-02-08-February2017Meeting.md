* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 8, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_February2017Meeting_02082017.pdf)

[Minutes](../../minutes/NSWC_minutes_March2017Meeting_02082017_signed.pdf)

[Packet](../../materials/NSWC_packet_February2017Meeting_02082017_postmeeting.pdf)
