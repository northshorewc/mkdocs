<!-- 2017-10-11-October2017Meeting.md -->
* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 11, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_October2017Meeting_10112017.pdf)

<!-- Minutes *-- official minutes not available at this time* -->
[Minutes](../../minutes/NSWC_minutes_November2017Meeting_10112017_signed.pdf)

[Packet](../../materials/NSWC_packet_October2017Meeting_10102017_postmeeting.pdf)

* * *
## Agenda Items

1. Call to order by the Chairman.
* Approval of the minutes of the October meeting.
* Monthly report of plant operations.
* Discussion of annual operating budget.
* Approval of monthly bills and estimated invoices.
* Status report of current capital projects. Action may be taken to approve capital expenditures and authorize payment to vendors.
* Discussion and possible action to accept chemical quotations for 2018.
* Discussion and possible action to approve 5 and 20 year capital improvement plans.
* Manager’s report.
* Date and time of the next regular Commission Meeting.
* Adjournment.

* * *
## Minutes (Unofficial)

A meeting of the North Shore Water Commission was held at the Filtration Plant, 400 West Bender Road, Glendale, Wisconsin on Wednesday, October 11, 2017.

Meeting was called to order at 8:00 A.M. by Mr. Botcher.

Present:	Scott Botcher, Chair; John Edlebeck, Member; Dave Eastman, Secretary; Rachel Reiss, Alternate for Glendale

Also present:	Eric Kiefer, Plant Manager & Recording Secretary

Absent:	Karen Schapiro, Alternate for Fox Point; Paul Boening, Alternate for Whitefish Bay

### MINUTES

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the minutes for the meeting held September 13, 2017.

### MONTHLY REPORT OF PLANT OPERATIONS

Mr. Kiefer provided the Commission with a report regarding plant operations. Pumpage in September is higher than last September; however, the year-to-date pumpage is still lower than last year. Mr. Kiefer expressed some concern that electrical efficiency is lower than expected, as was the case for August. He reported that plant staff is investigating possible causes.

The report was placed on file without any motion.

### ANNUAL OPERATING BUDGET

Mr. Kiefer presented the monthly financial reports and they were put on file without motion.

### MONTHLY BILLS

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried that the following payments be approved and authorization was given to the Fiscal Agent to make such payments:

[List of Invoices](https://goo.gl/PG2wKL)

### CAPITAL PROJECTS

Mr. Kiefer provided a brief report about the capital improvement projects. First, Mr. Kiefer reported that plant staff is continuing its efforts to set up the new SCADA system, and that a contractor will be assisting in the month of October. Secondly, 1 hour of labor was charged to the Filter Improvements Project. Lastly, SEH submitted an invoice for work related to groundwater monitoring around reservoirs, as part of the Reservoir Improvement Project.

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the payment request for the SCADA Upgrade Project in the amount of $1,399.31, the payment request for the Filter Improvement Project in the amount of $34.37, and the payment request for the Reservoir Improvement Project in the amount of $5,590.47 as per Mr. Kiefer’s memos dated October 11, 2017.

### CAPITAL IMPROVEMENT PLANS

Mr. Kiefer provided the Commission with a 5-year and 20-year capital improvement plan. He explained that in 2038 the generator at Klode Park would be replaced; otherwise, everything else remained the same.

Mr. Eastman asked Mr. Kiefer to consider moving the Port Washington Road & Bradley Road Interconnection project from 2036 to a different year. He explained that the project would improve all 3 systems and is important. Mr. Kiefer asked for a suggestion as to when the project should be completed. Alternate Reiss suggested that years 2024 and 2025 could be combined into a single project--for 2024. The aforementioned interconnection could then be moved to 2025. Mr. Eastman agreed.

Mr. Kiefer explained that he would make the requested changes and bring back to the Commission next month.

No action was taken regarding this matter.

### ACKNOWLEDGMENT AND ACCEPTANCE OF NEW AGREEMENT

Mr. Kiefer displayed the Revised and Restated Agreement for Water Supply that has been executed by all 3 member communities. He mentioned that the agreement is effective the date the last member signed, which was September 18, 2017.

Mr. Botcher asked if the Commission has a fireproof safe. Mr. Kiefer replied that the document would be kept in its fireproof cabinet.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously approved to accept the Revised and Restated Agreement for Water Supply Agreement effective 9/18/2017.

### MANAGER’S REPORT

1. Operations staff started a new schedule that involves a 4-week rotation and sharing relief duties.
2. Basin 3 was cleaned, inspected, and put back into service.
3. Basin 4 was cleaned, inspected, and put back into service after minor repairs were made to boards on flocculator.
4. Open House events were held on September 16 and September 20.
5. Plant staff replaced a faulty temperature sensor in the dehumidification system.
6. Plant staff started a long-term bolt replacement project in the filter wing.
7. Kirk Hallberg started as Intern on September 26.

### NEXT MEETING

The next regular meeting was scheduled for Wednesday, November 8 at 8:00 AM.

### ADJOURNMENT

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to adjourn at 8:25 A.M.

Submitted by Eric Kiefer, Plant Manager and Recording Secretary
