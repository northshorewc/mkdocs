* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 16, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_August2017Meeting_08162017.pdf)

[Minutes](../../minutes/NSWC_minutes_September2017Meeting_08162017_signed.pdf)

[Packet](../../materials/NSWC_packet_August2017Meeting_08162017_postmeeting.pdf)
