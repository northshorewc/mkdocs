<!-- 2017-10-11-October2017Meeting.md -->
* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 13, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_December2017Meeting_12132017.pdf)

<!-- Minutes *-- official minutes not available at this time* -->
[Minutes](../../minutes/NSWC_minutes_January2018Meeting_12132017_signed.pdf)

[Packet](../../materials/NSWC_packet_December2017Meeting_12132017_postmeeting.pdf)

* * *
## Agenda Items

1. Call to order by the Chairman.
* Approval of the minutes of the November meeting.
* Monthly report of plant operations.
* Discussion of annual operating budget.
* Approval of monthly bills and estimated invoices.
* Status report of current capital projects. Action may be taken to approve capital expenditures and authorize payment to vendors.
* Discussion of new website and possible action to make changes as determined during discussion.
* Manager’s report.
* Date and time of the next regular Commission Meeting.
* Adjournment.

* * *
## Minutes (*UNOFFICIAL*)

<!-- ## Minutes
*Not available at this time.* -->

A meeting of the North Shore Water Commission was held at the Filtration Plant, 400 West Bender Road, Glendale, Wisconsin on Wednesday, December 13, 2017.

Meeting was called to order at 8:00 A.M. by Mr. Botcher.

Present:	Scott Botcher, Chair (Fox Point); Paul Boening, Alternate (Whitefish Bay); Dave Eastman, Secretary (Glendale); Rachel Reiss, Alternate (Glendale)

Also present:	Eric Kiefer, Plant Manager & Recording Secretary; Charlie Imig

### MINUTES
It was moved by Mr. Eastman, seconded by Mr. Boening, and unanimously carried to approve the [minutes](../../minutes/NSWC_minutes_December2017Meeting_11082017_signed.pdf) for the meeting held November 8, 2017.

### MONTHLY REPORT OF PLANT OPERATIONS
Mr. Kiefer provided the Commission with a report regarding plant operations. Besides production being 3 million gallons lower than last November, all of the operating figures appear to be normal. The [report](../../monthly-reports/NSWC_report_MonthlyReportOfOperations_November2017_01042018_revised.pdf) was placed on file without any motion.

### ANNUAL OPERATING BUDGET
Mr. Kiefer presented the monthly [financial reports](../../financials/NSWC_financials_December2017Meeting_12082017.pdf), and they were put on file without motion.

### MONTHLY BILLS
It was moved by Mr. Eastman, seconded by Mr. Boening, and unanimously carried that the following payments be approved and authorization was given to the Fiscal Agent to make such payments:

| Vendor                                                                                                                                                                                            |     Amount |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------:|
| A.C. Engineering (maintenance of electrical systems)                                                                                                                                              |  $2,354.20 |
| Alexander Chemical (treatment chemical: sodium hypochlorite)                                                                                                                                      |  $2,768.00 |
| Batteries Plus (lamp disposal)                                                                                                                                                                    |      $9.84 |
| BMO Harris Bank (credit card - Operating Fund)                                                                                                                                                    |    $715.57 |
| -- Amazon (hard drives and hard drive holders): $401.00                                                                                                                                           |            |
| -- AT&T (monthly charge for internet and one-time installation fees): $165.51                                                                                                                     |            |
| -- Google (monthly charge for Google Suite Basic): $54.16                                                                                                                                         |            |
| -- Straight Talk (monthly phone plan - Foreman): $45.90                                                                                                                                           |            |
| -- USPS (postage): $49.00                                                                                                                                                                         |            |
| Diversified Benefit Services (Section 125 Plan administration)                                                                                                                                    |     $95.00 |
| Eurofins (compliance monitoring)                                                                                                                                                                  |    $380.00 |
| Fastenal (nuts and bolts)                                                                                                                                                                         |    $386.58 |
| Fuchs and Boyle (legal services)                                                                                                                                                                  |     $19.50 |
| Grainger (custodial supplies, electrical supplies, control transformer, mop and bucket, first aid supplies, fuses, lab reagents, light fixtures and bulbs, paint, plumbing supplies, and filters) |  $2,289.60 |
| Great America (lease payment for copier/printer)                                                                                                                                                  |    $100.00 |
| Hach (phosphate reagent)                                                                                                                                                                          |     $72.94 |
| Hawkins (treatment chemicals: aqueous ammonia, polymer, and phosphates)                                                                                                                           |  $5,044.93 |
| Idexx (collection vessels, comparators, and quality control media)                                                                                                                                |    $875.33 |
| Lincoln Contractors Supply (masonry saw blade)                                                                                                                                                    |    $215.99 |
| McMaster-Carr (stainless steel screen and electrical terminals)                                                                                                                                   |     $62.70 |
| Minnesota Life / Securian (employee life insurance)                                                                                                                                               |    $128.92 |
| Mulcahy Shaw Water (reagents for chlorine analyzer)                                                                                                                                               |    $452.65 |
| Northern Lake Service (compliance monitoring)                                                                                                                                                     |     $67.00 |
| Office Copying Equipment (maintenance payment for copier/printer)                                                                                                                                 |     $18.00 |
| Quill (office supplies, pens, and pencils)                                                                                                                                                        |     $59.19 |
| Rotroff Jeanson (monthly accounting services)                                                                                                                                                     |  $1,010.00 |
| Spectrum Business (internet services, Bender phone, and Klode phone)                                                                                                                              |    $491.68 |
| Superior Chemical (hand soap)                                                                                                                                                                     |    $106.12 |
| Swanson Flo (repair of valve actuator)                                                                                                                                                            |    $971.15 |
| T-Mobile (mobile internet)                                                                                                                                                                        |     $31.05 |
| US Cellular (cellular phone service)                                                                                                                                                              |      $4.25 |
| USA Bluebook (phosphate reagent)                                                                                                                                                                  |    $330.90 |
| Village Ace (hardware and hose)                                                                                                                                                                   |     $63.92 |
| Village of Fox Point (gasoline)                                                                                                                                                                   |     $61.12 |
| We Energies (Bender Electric)                                                                                                                                                                     | $13,363.41 |
| We Energies (Bender Gas)                                                                                                                                                                          |  $1,206.03 |
| We Energies (Green Tree Electric)                                                                                                                                                                 |     $18.16 |
| We Energies (Henry Clay Electric)                                                                                                                                                                 |     $19.23 |
| We Energies (Klode Electric)                                                                                                                                                                      |  $3,872.51 |
| We Energies (Klode Gas)                                                                                                                                                                           |     $20.10 |
| WDATCP (renewal of water lab license)                                                                                                                                                             |  $1,020.00 |
| Wilkens Anderson (petri dishes)                                                                                                                                                                   |     $90.72 |
| Wisconsin State Lab of Hygiene (fluoride analysis)                                                                                                                                                |     $25.00 |
|                                                                                                                                                                                                   |            |
| **SUB-TOTAL**                                                                                                                                                                                     | $38,821.29 |
| *Maintenance Reserve*                                                                                                                                                                             |            |
|                                                                                                                                                                                                   |            |
| **SUB-TOTAL**                                                                                                                                                                                     |      $0.00 |
|                                                                                                                                                                                                   |            |
| **TOTAL**                                                                                                                                                                                         | $38,821.29 |

### CAPITAL PROJECTS
Mr. Kiefer provided a brief report about the capital improvement projects. First, Mr. Kiefer reported that plant staff is continuing its efforts to set up the new HMI and database system. He provided an overview of the project and explained that plant staff and the contractor have encountered several problems that have slowed progress. Those issues have been resolved, and Mr. Kiefer hopes the project will be completed on time.

Second, Mr. Kiefer provided an overview of the filter upgrade project and reported that plant staff is continuing to install new filter actuators and differential pressure transducers. Mr. Kiefer was confident that this project will be completed on time.

Third, Mr. Kiefer provided an overview of the chemical feed project and reported that plant staff is working on demolishing the old chemical feed room. There has been significant progress over the past month, and Mr. Kiefer believes the project will be completed on time.

It was moved by Mr. Boening, seconded by Mr. Eastman, and unanimously carried to approve the [payment requests](../../payment-requests/NSWC_paymentRequests_December2017Meeting_12132017.pdf) as per Mr. Kiefer’s memos dated December 13, 2017.

### WEBSITE UPDATE
Mr. Kiefer explained that a new website was developed and is in use. He attempted to show the audience using the TV and laptop computer; however, there were some technical difficulties. He wasn’t able to show much of the new website. Mr. Kiefer did ask about the posting of minutes. It was the consensus of the Commission that the minutes could be posted as part of the packet, but should not be put on the website as “minutes” until approved by the Commission.
No action was taken regarding this matter.

### MANAGER’S REPORT
1. The plant dehumidification system stopped working in early November. Plant staff determined that a compressor heater failed along with several control components. Repairs were made, and the system was put back into service by the end of November.
* A.C. Engineering performed routine maintenance on the high voltage transformers, switchgear, and other electrical cabinets throughout the plant.
* Plant staff replaced the lamps in UV reactor #2 along with other seals and components.
* Plant staff is continuing to wax floors throughout the plant.
* Plant staff replaced all of the mercury vapor lamps in the old chemical feed room with LED lamps.
* One of the Spectrum internet connections was replaced with an AT&T internet connection.
* The Commission participated in a long-term Water Research Foundation (WRF) project from 2014 to 2016. That project concluded and the report is available to download from the WRF website. The project is entitled “Optimization of Phosphorus-Based Corrosion Control Chemicals Using a Comprehensive Perspective of Water Quality” and has a project number of 4586.

At this time, Charlie Imig was introduced as Mr. Eastman’s successor at the City of Glendale. The Commission welcomed Mr. Imig.

### NEXT MEETING
The next regular meeting was scheduled for Wednesday, January 10, 2018 at 8:00 AM.

### ADJOURNMENT
It was moved by Mr. Boening, seconded by Mr. Eastman, and unanimously carried to adjourn at 8:25 A.M.
