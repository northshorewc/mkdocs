<!-- 2017-10-11-October2017Meeting.md -->
* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 8, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_November2017Meeting_11082017.pdf)

[Minutes](../../minutes/NSWC_minutes_December2017Meeting_11082017_signed.pdf)

[Packet](../../materials/NSWC_packet_November2017Meeting_11082017_postmeeting.pdf)

* * *
## Agenda Items

1. Call to order by the Chairman.
* Approval of the minutes of the October meeting.
* Monthly report of plant operations.
* Discussion of annual operating budget.
* Approval of monthly bills and estimated invoices.
* Status report of current capital projects. Action may be taken to approve capital expenditures and authorize payment to vendors.
* Discussion and possible action to accept chemical quotations for 2018.
* Discussion and possible action to approve 5 and 20 year capital improvement plans.
* Manager’s report.
* Date and time of the next regular Commission Meeting.
* Adjournment.

* * *
## Minutes (*UNOFFICIAL*)

A meeting of the North Shore Water Commission was held at the Filtration Plant, 400 West Bender Road, Glendale, Wisconsin on Wednesday, November 8, 2017.

Meeting was called to order at 8:00 A.M. by Mr. Botcher.

Present:	Scott Botcher, Chair; John Edlebeck, Member; Dave Eastman, Secretary

Also present:	Eric Kiefer, Plant Manager & Recording Secretary

### MINUTES

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the [minutes](../../minutes/NSWC_minutes_November2017Meeting_10112017_signed.pdf) for the meeting held October 11, 2017.

### MONTHLY REPORT OF PLANT OPERATIONS

Mr. Kiefer provided the Commission with a report regarding plant operations. He explained that pumpage in October is higher than last October because of hot and dry weather in the beginning of the month. Mr. Kiefer also mentioned that plant staff switched to a different pumping / filling schedule which should improve efficiency of the high service pumps.

The [report](../../monthly-reports/NSWC_report_MonthlyReportOfOperations_October2017_01042018_revised.pdf) was placed on file without any motion.

### ANNUAL OPERATING BUDGET

Mr. Kiefer presented the [monthly financial reports](../../financials/NSWC_financials_November2017Meeting_11082017.pdf), and they were put on file without motion.

### MONTHLY BILLS

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried that the following payments be approved and authorization was given to the Fiscal Agent to make such payments:

| Vendor                                                                                                                             |         Amount |
|:-----------------------------------------------------------------------------------------------------------------------------------|---------------:|
| Alexander Chemical (treatment chemical: sodium hypochlorite)                                                                       |      $2,770.77 |
| BMO Harris Bank (credit card - Operating Fund)                                                                                     |        $115.01 |
| -- Google (monthly charge for Google Suite Basic): $54.16                                                                          |                |
| -- Siteground (domain registration renewal): $14.95                                                                                |                |
| -- Straight Talk (monthly phone plan - Foreman): $45.90                                                                            |                |
| Diversified Benefit Services (Section 125 Plan administration)                                                                     |         $95.00 |
| Duane Ziege (reimb for conference fee and certification renewal)                                                                   |         $65.00 |
| Eurofins (compliance monitoring)                                                                                                   |        $380.00 |
| Fastenal (nuts and bolts)                                                                                                          |        $386.59 |
| Fuchs and Boyle (legal services)                                                                                                   |         $19.50 |
| Grainger (custodial supplies, fuses, lab reagents, light fixtures and bulbs, paint, caulk, cables, plumbing supplies, and filters) |        $618.52 |
| Great America (lease payment for copier/printer)                                                                                   |        $100.00 |
| Hawkins (treatment chemicals: aqueous ammonia)                                                                                     |        $435.97 |
| Idexx (Colilert)                                                                                                                   |      $2,271.47 |
| MMSD (3rd quarter sludge disposal)                                                                                                 |      $1,924.51 |
| Minnesota Life / Securian (employee life insurance)                                                                                |        $128.92 |
| Northern Lake Service (compliance monitoring)                                                                                      |         $67.00 |
| Office Copying Equipment (maintenance payment for copier/printer)                                                                  |         $22.60 |
| Outdoor Lighting (time and material to repair light poles)                                                                         |        $975.05 |
| Rotroff Jeanson (monthly accounting services)                                                                                      |      $1,010.00 |
| Spectrum Business (internet services, Bender phone, and Klode phone)                                                               |        $680.03 |
| Starnet Technologies (NCC data charge for 4th quarter)                                                                             |        $300.00 |
| Superior Chemical (floor stripper, floor finish, glass cleaner, and paper products)                                                |        $504.77 |
| T-Mobile (mobile internet)                                                                                                         |         $31.05 |
| US Cellular (cellular phone service)                                                                                               |          $4.25 |
| USA Bluebook (chlorine reagent)                                                                                                    |        $518.83 |
| Village Ace Hardware (grass seed)                                                                                                  |         $17.09 |
| Village of Fox Point (gasoline)                                                                                                    |         $59.95 |
| We Energies (Bender Electric)                                                                                                      |     $14,550.59 |
| We Energies (Bender Gas)                                                                                                           |        $281.29 |
| We Energies (Green Tree Electric)                                                                                                  |         $15.99 |
| We Energies (Henry Clay Electric)                                                                                                  |         $16.92 |
| We Energies (Klode Electric)                                                                                                       |      $3,753.79 |
| We Energies (Klode Gas)                                                                                                            |         $18.50 |
| Whitefish Bay Water Utility (water bill at Klode Park)                                                                             |        $241.74 |
| Wisconsin State Lab of Hygiene (fluoride analysis & enrollment in 2018 water mircobiology certification program)                   |        $633.00 |
| **SUB-TOTAL**                                                                                                                      | **$33,013.70** |
|                                                                                                                                    |                |
| *Maintenance Reserve*                                                                                                              |                |
| **SUB-TOTAL**                                                                                                                      |      **$0.00** |
|                                                                                                                                    |                |
| **TOTAL**                                                                                                                          | **$33,013.70** |

### CAPITAL PROJECTS

Mr. Kiefer provided a brief report about the capital improvement projects. First, Mr. Kiefer reported that plant staff is continuing its efforts to set up the new SCADA system HMI. He explained that the process is going slower than expected due to extra effort required to address unique characteristics of Bristol hardware and associated software.

Second, Mr. Kiefer explained that plant staff is working on installing new filter actuators as well as differential pressure transducers for selected flow meters. He mentioned that they found a valve that did not operate correctly. Quotations have been solicited for a replacement valve.

Third, Mr. Kiefer mentioned that plant staff continues to work on demolishing the old chemical feed room as time permits.

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the payment request for the SCADA Upgrade Project in the amount of $6,157.18, the payment request for the Filter Improvement Project in the amount of $2,576.34, and the payment requestfor the Chemical Feed Project Upgrade in the amount of $204.30 as per Mr. Kiefer’s [memos](../../payment-requests/NSWC_paymentRequests_November2017Meeting_11072017.pdf) dated November 7, 2017.

### CHEMICAL QUOTATIONS

Mr. Kiefer presented his recommendations for acceptance of chemical quotations. He mentioned that all unit costs are the same or lower than last year, with the exception of sodium hypochlorite which increased 2.3% from last year.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to accept the chemical quotations as noted in Mr. Kiefer’s [memo](../../chemicals/NSWC_chemicalQuotations_November2017Meeting_11082017.pdf) dated November 1, 2017.

#### Accepted Quotations:
* Hawkins: ammonium hydroxide (19%) - $0.240/lb
* Olin: sodium hypochlorite (12.5%) - $0.710/gal
* Rowell: hydrofluorosilicic acid (23%) - $0.164/lb
* ChemTrade: liquid Aluminum Sulfate - $263/dry ton
* Hawkins: phosphate (LPC-132) - $0.420/lb
* Hawkins: polymer (Flocculation Aid AquaHawk 6527) - $0.65/lb
* Nalco: Veligon TL-M (Mussel Control) - $1.11/lb

### CAPITAL IMPROVEMENT PLANS

Mr. Kiefer provided the Commission with a 5-year and 20-year capital improvement plan. He explained that the plans are the same as the ones presented last month with the following exceptions. First, he fixed the spelling of the word “capital,” which was originally misspelled throughout the documents. Second, he placed the new interconnection project at 2025 as proposed last month by Mr. Eastman. Third, he placed an HVAC upgrade at year 2036 to fill the gap. Mr. Kiefer explained that the project could be scheduled sooner if the original 1962 equipment fails.

Mr. Edlebeck asked about financing the 2020 capital improvement project. He asked the other Commissioners if it would be beneficial to have a single entity raise the capital and make arrangements for the other members to reimburse that entity--much like the North Shore Fire Department. Mr. Botcher discussed what factors should be evaluated when raising capital for this kind of a project. After discussion, there was agreement that it would be best if each member raised their capital commitment independently.

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the [5-Year](../../cips/NSWC_capitalImprovementPlan_2019to2023_11082017_Approved.pdf) and [20-Year](../../cips/NSWC_capitalImprovementPlan_2019to2038_11082017_Approved.pdf) capital improvement plans prepared by Mr. Kiefer dated November 3, 2017.

### MANAGER’S REPORT

1. Basin #2 was cleaned and inspected. After minor repairs were made, it was put back into service.
* The northwest reservoir was put back into service after it was successfully inspected by SEH. Plant staff intends to make minor repairs suggested by SEH before the next inspection in 2018.
* Plant staff successfully replaced the actuator on the left effluent valve on filter #4 during the capital project. However, in the process of doing the project, staff discovered the valve itself does not seat properly. Arrangements are being made for the valve to be replaced.
* The Veligon TL-M chemical feed system at Klode Park has been turned off in preparation for winter operations.
* Plant staff transitioned to winter tower/standpipe fill schedule.
* Plant staff is developing new website that will likely be ready by December.

### NEXT MEETING

The next regular meeting was scheduled for Wednesday, December 13, 2017 at 8:00 AM.

### ADJOURNMENT

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to adjourn at 8:30 A.M.
