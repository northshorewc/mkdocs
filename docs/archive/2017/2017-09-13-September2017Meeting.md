* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 13, 2017** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_September2017Meeting_09132017.pdf)

[Minutes](../../minutes/NSWC_minutes_October2017Meeting_09132017_signed.pdf)

[Packet](../../materials/NSWC_packet_September2017Meeting_09132017_postmeeting.pdf)

* * *
## Agenda Items

1. Call to order by the Chairman.
* Approval of the minutes of the August meeting.
* Monthly report of plant operations.
* Discussion of annual operating budget.
* Approval of monthly bills and estimated invoices.
* Status report of current capital projects. Action may be taken to approve capital expenditures and authorize payment to vendors.
* Discussion and possible approval to allow Plant Manager to sell unused groundskeeping equipment.
* Discussion and possible action to change bank from BMO Harris Bank to Waterstone Bank.
* Discussion and possible approval of the 2018 Operation Budget and 2018 Capital Improvement Budget.
* Discussion of the Revised and Restated Agreement for Water Supply System.
* Manager’s report.
* Date and time of the next regular Commission Meeting.
* Adjournment.

* * *
## Minutes (unofficial)

A meeting of the North Shore Water Commission was held at the Filtration Plant, 400 West Bender Road, Glendale, Wisconsin on Wednesday, September 13, 2017.

Meeting was called to order at 8:02 A.M. by Mr. Botcher.

Present:	Scott Botcher, Chair; John Edlebeck, Member; Dave Eastman, Secretary; Rachel Reiss, Alternate for Glendale

Also present:	Eric Kiefer, Plant Manager & Recording Secretary

Absent:	Karen Schapiro, Alternate for Fox Point; Paul Boening, Alternate for Whitefish Bay

### APPROVAL OF MINUTES

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the minutes for the meeting held August 16, 2017.

### MONTHLY REPORT OF PLANT OPERATIONS

Mr. Kiefer provided the Commission with a report regarding plant operations. Mr. Edlebeck asked about the KWH/MG figure listed in Mr. Kiefer’s report. Mr. Kiefer explained that when pumps operate at low speeds, as they have this summer, they operate less efficiently. He suspects that is why the KWH/MG figure is higher than normal. He explained that he would research the matter and get back to Mr. Edlebeck.

The report was placed on file without any motion.

### ANNUAL OPERATING BUDGET

Mr. Kiefer presented the monthly financial reports and they were put on file without motion.

Mr. Botcher mentioned at this time that We Energies rates would not go up next year, but may increase steeply in 2 to 3 years.

### MONTHLY BILLS

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried that the following payments be approved and authorization was given to the Fiscal Agent to make such payments:

[List of Invoices](https://goo.gl/6a79pX)

### CAPITAL PROJECTS

Mr. Kiefer provided a brief report about the capital improvement projects. He explained that plant staff has spent the last 2 months aggressively working on sandblasting and painting the filter room ceiling. Some progress was made in programming the SCADA system and in demolishing equipment in the old chemical feed room.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to approve the payment request for the SCADA Upgrade Project in the amount of $1,846.57 and the payment request for the Chemical Feed Upgrade Project in the amount of $247.49 as per Mr. Kiefer’s memos dated September 13, 2017.

### SALE OF UNUSED GROUNDSKEEPING EQUIPMENT

Mr. Kiefer explained that there is a pickup mower and a mower deck that is no longer used. He requested that the Commission authorize the sale of those pieces of equipment.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to authorize the Plant Manager to sell the surplus mowers as identified in Mr. Kiefer’s memo dated 9/12/2017.

### COST BENEFIT ANALYSIS OF SWITCHING BANK ACCOUNTS

Mr. Kiefer provided the Commission with a brief memo regarding the costs and benefits associated with switching bank accounts from BMO Harris Bank to WaterStone Bank. Both banks offered the the same services required by the Commission at approximately the same cost. WaterStone Bank, however, appeared to offer better customer service. Since Mr. Kiefer did not find there was any urgency in switching immediately to a different bank, he thought it would be best to defer any action until the inventory of checks decreased.

Ms. Reiss and Mr. Botcher suggested that Mr. Kiefer research multiple banks. Mr. Kiefer explained that he intends to thoroughly investigate multiple banks in winter or spring of 2018.

No action was taken regarding this matter.

### 2018 BUDGETS

Mr. Kiefer provided the Commission with an operating budget and capital improvement budget for 2018. The proposed operating budget was in the amount of $1,321,650 (0.5% increase) and the capital improvement budget was in the amount of $187,271 (9.3% decrease). He explained how additional information--regarding how the budget would be allocated to each member--was added to the budget pursuant to the revised and restated agreement.

Mr. Kiefer also provided the Commission with a form letter he intends to send each Village Manager and City Administrator which explains how the budget affects each member.

After brief discussion, it was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to approve the 2018 Operating Budget and 2018 Capital Improvement Budget dated 9/11/2017.

### REVISED AND RESTATED AGREEMENT

Mr. Kiefer reported that the City of Glendale approved and signed the revised and restated agreement, and that the Village of Fox Point took action to approve the agreement on Tuesday, September 12.

Mr. Edlebeck announced that the Village of Whitefish Bay will discuss and possibly approve it at the meeting on Monday, September 18.

No action was taken regarding this matter.

### MANAGER’S REPORT

1. A.C. Engineering repaired and installed breaker on the generator side of the transfer switch at the Bender facility.
* Plant staff replaced the refrigerator in the lab because it was not maintaining a constant temperature.
* Plant staff repaired a leak in the ammonia feed room and in the sodium hypochlorite bulk containment area.
* Outdoor Lighting replaced lamps that were burnt on light poles by the north parking lot and the east reservoirs.
* Plant staff painted the meter testing apparatus piping in basement.
* Plant staff sandblasted and painted the middle section of the filter room ceiling.
* Plant staff is cleaning in preparation for the open house on 9/16 and 9/20.
* Russell Koenig completed the summer internship on August 25.

Mr. Eastman announced that the Sears building will be demolished in the near future and that the Commission should be concerned about the raw water main that is close to that property.

### NEXT MEETING

The next regular meeting was scheduled for Wednesday, October 11 at 8:00 AM.

### ADJOURNMENT
It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to adjourn at 8:38 A.M.

Submitted by:
Eric Kiefer, Plant Manager and Recording Secretary	Date
