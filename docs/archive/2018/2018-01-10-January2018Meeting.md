<!-- 2017-10-11-October2017Meeting.md -->
* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 10, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

## Agenda Items

1. Call to order by the Chairman.
* Approval of the minutes of the December meeting.
* Monthly report of plant operations.
* Discussion of annual operating budget.
* Approval of monthly bills.
* Status report of current capital projects. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
* Discussion of 2017 capital additions and retirements. Possible action may be taken regarding this matter.
* Discussion and possible approval of the Control System Support and Preventative Maintenance Agreement with Starnet Technologies.
* Discussion and possible action to select quotation for UCMR4 testing in 2018 and 2019.
* Discussion of annual report and revisions made to 2017 monthly reports.
* Manager’s report.
* Date and time of the next regular Commission Meeting.
* Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2018Meeting_01102018.pdf)

* * *
<!-- ## Minutes (*UNOFFICIAL*) -->
## Minutes

A meeting of the North Shore Water Commission was held at the Filtration Plant, 400 West Bender Road, Glendale, Wisconsin on Wednesday, January 10, 2018.

Meeting was called to order at 8:00 A.M. by Mr. Botcher.

Present:	Scott Botcher, Chair (Fox Point); Dave Eastman, Secretary (Glendale)

Also present:	Eric Kiefer, Plant Manager & Recording Secretary; Charlie Imig

### MINUTES
It was moved by Mr. Eastman, seconded by Mr. Botcher, and unanimously carried to approve the [minutes](../../minutes/NSWC_minutes_January2018Meeting_12132017_signed.pdf) for the meeting held December 13, 2017.

Member John Edlebeck arrived at 8:04 AM to represent Whitefish Bay.

### MONTHLY REPORT OF PLANT OPERATIONS
Mr. Kiefer provided the Commission with a report regarding plant operations. He mentioned that pumpage was down this year over last year. Overall, the percentage of water delivered to Glendale went down, and the percentage of water delivered to the other members went up. The [report](../../monthly-reports/NSWC_report_MonthlyReportOfOperations_January2018Meeting_01092018.pdf) was placed on file without any motion.

### ANNUAL OPERATING BUDGET
Mr. Kiefer presented the monthly [financial reports](../../financials/NSWC_financials_January2018Meeting_01052018.pdf), and they were put on file without motion.

### MONTHLY BILLS
It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried that the following payments be approved and authorization was given to the Fiscal Agent to make such payments:

| Vendor                                                                                                        |     Amount |
|:--------------------------------------------------------------------------------------------------------------|-----------:|
| Batteries Plus (12V battery)                                                                                  |    $174.95 |
| BMO Harris Bank (credit card - Operating Fund)                                                                |    $352.25 |
| -- AT&T (monthly charge for internet and one-time installation fees): $58.99                                  |            |
| -- Google (monthly charge for Google Suite Basic): $54.16                                                     |            |
| -- Straight Talk (annual website hosting services): $193.20                                                   |            |
| -- Straight Talk (monthly phone plan - Foreman): $45.90                                                       |            |
| Cintas Fire Protection (fire extinguisher inspection and testing)                                             |    $383.25 |
| City of Milwaukee (standby service - 6 months)                                                                |  $1,350.00 |
| Diversified Benefit Services (Section 125 Plan administration)                                                |     $95.46 |
| Eurofins (compliance monitoring)                                                                              |    $380.00 |
| Faust Company (mechanical contractor services to fix refrigerant leak and replenish refrigerant)              |  $5,825.00 |
| Fuchs and Boyle (legal services)                                                                              |     $19.50 |
| Glendale Water Utility (quarterly storm water and environmental charges)                                      |    $591.60 |
| Grainger (conduit fittings, custodial supplies, belts, screws, nuts, bolts, respirators, and office supplies) |    $652.17 |
| Great America (lease payment for copier/printer)                                                              |    $100.00 |
| Hawkins (treatment chemicals: aqueous ammonia and phosphates)                                                 |    $907.20 |
| Home Depot (demolition hammer rental)                                                                         |    $117.33 |
| Key Benefit Concepts (OPEB table update)                                                                      |    $350.00 |
| Minnesota Life / Securian (employee life insurance)                                                           |    $128.92 |
| MMSD (4th quarter sludge disposal)                                                                            |  $1,851.24 |
| Northern Lake Service (compliance monitoring)                                                                 |    $627.00 |
| Office Copying Equipment (maintenance payment for copier/printer)                                             |     $21.56 |
| Quill (office supplies)                                                                                       |    $194.00 |
| Rotroff Jeanson (monthly accounting services)                                                                 |  $1,010.00 |
| SEH (inspection of reservoir #2)                                                                              |  $2,880.00 |
| Spectrum Business (internet services, Bender phone, and Klode phone)                                          |    $464.33 |
| The Water Council (annual membership renewal)                                                                 |  $1,000.00 |
| T-Mobile (mobile internet)                                                                                    |     $31.05 |
| US Cellular (cellular phone service)                                                                          |      $4.25 |
| USA Bluebook (chemical feed pump tube replacement & turbidimeter plug)                                        |    $152.63 |
| Village Ace (hardware)                                                                                        |     $21.96 |
| Village of Whitefish Bay (Klode water bill)                                                                   |    $179.83 |
| Water Research Foundation (annual membership renewal)                                                         |  $3,110.16 |
| We Energies (Bender Electric)                                                                                 | $13,777.67 |
| We Energies (Bender Gas)                                                                                      |  $1,897.90 |
| We Energies (Green Tree Electric)                                                                             |     $17.08 |
| We Energies (Henry Clay Electric)                                                                             |     $18.01 |
| We Energies (Klode Electric)                                                                                  |  $3,785.76 |
| We Energies (Klode Gas)                                                                                       |    $109.00 |
| Wisconsin State Lab of Hygiene (fluoride analysis)                                                            |     $25.00 |
|                                                                                                               |            |
| **SUB-TOTAL**                                                                                                 | $42,606.06 |
| *Maintenance Reserve*                                                                                         |            |
|                                                                                                               |            |
| **SUB-TOTAL**                                                                                                 |      $0.00 |
|                                                                                                               |            |
| **TOTAL**                                                                                                     | $42,606.06 |

### CAPITAL PROJECTS
Mr. Kiefer provided a brief report about the capital improvement projects. He reported that the SCADA project, for phase 1, was nearly completed. This project is budgeted to extend into 2018 with the major emphasis on preventing data loss during network interruptions. The other capital improvement projects were substantially completed on time.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to approve the [payment requests](../../payment-requests/NSWC_paymentRequests_January2018Meeting_01102018.pdf) as per Mr. Kiefer’s memos dated January 10, 2018.

### 2017 CAPITAL ADDITIONS AND RETIREMENTS
Mr. Kiefer provided the Commission with a summary of capital additions and retirements in 2017. He mentioned that all assets are kept on the books of each member utility and that this information will be included in the financial audit. Mr. Kiefer noted that this report is preliminary but is necessary to inform the member municipalities of additions and retirements that have occurred throughout the year.

### CONTROL SYSTEM SUPPORT AND PREVENTATIVE MAINTENANCE AGREEMENT
Mr. Kiefer provided the Commission with a renewal agreement for control system support and preventative maintenance with Starnet Technologies. He said the agreement has been renewed for many years and is valuable to the Commission. He recommends that the Commission approve the renewal.

Mr. Edlebeck commented that the agreement is set to automatically renew if action isn’t taken 30 days prior to the end of the agreement. Mr. Kiefer acknowledged that and said he is okay with that. He may work with contractor to change agreement next year so that agreement does not automatically renew.

Mr. Kiefer also noted that any unused contracted time gets carried over to the following year.

Mr. Botcher stated the agreement does not indicate which CPI index should be used when calculating increases in the renewal year. He suggested the contract could be revised to clear up this ambiguity.

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the [Control System Support and Preventative Maintenance](../../agreements/StarnetTechnologies_agreement_ControlSystemSupportAndPreventativeMaintenance_01102018.pdf) with Starnet Technologies for a lump sum of $12,389.00.

### UCMR4 PROPOSALS
Mr. Kiefer explained that the Commission conducts Unregulated Contaminant Monitoring Rule (UCMR) monitoring as required by the United States Environmental Protection Agency (USEPA). Based on the size of its member water utilities, Glendale and Whitefish Bay were selected to participate from June 2018 through March 2019.

Mr. Kiefer explained how the monitoring regime is complicated and that additional testing could be required based on lab results. Consequently, each proposal included a contingency to cover such testing. After reviewing solicited proposals, Mr. Kiefer recommended that the Commission accept the lowest cost quotation from Pace Analytical Service, Inc.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to accept the [proposal](../../proposals/PaceAnalyticalServices_proposals_01082017.pdf) from Pace Analytical Services, Inc. dated January 9, 2018 for UCMR4 lab services in the amount of $13,840.00 with a contingency of $880.00 for any additional testing that may be required by the USEPA.

### ANNUAL REPORT AND REVISIONS TO 2017 MONTHLY REPORTS
Mr. Kiefer provided the Commission with the 2017 Annual Report. Although discussion of this report does not normally make it to the agenda, Mr. Kiefer thought it was necessary considering the monthly reports from January 2017 through November 2017 had to be revised to correct errors in the “Chemicals Applied” section.

Mr. Kiefer reminded the Commission that 2017 operating expenses will be apportioned based on delivery totals presented in this report.

The report was placed on file without any motion.

### MANAGER’S REPORT
1. The basement incoming fresh air preheat radiator froze and ruptured in early January. Plant staff removed that component and is searching for a contractor to repair or replace the damaged radiator.
2. Klode Park had to run on generator power for approximately 8 hours while utility power was down on New Year’s Eve.
The Wisconsin Department of Agriculture, Trade, and Consumer Protection (WDATCP) conducted its biennial inspection of the laboratory. No deviations were noted.
3. Plant staff met with SEH to discuss the upcoming reservoir project. At the July or August Commission Meeting, a presentation will be given to the Commission to discuss the details of the project and to answer questions.
4. Kirk Hallberg completed his internship on December 22. Staff intends to advertise and hire next intern by early to mid-February.

### NEXT MEETING
The next regular meeting was scheduled for Wednesday, February 14, 2018 at 8:00 AM.

### ADJOURNMENT
It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to adjourn at 8:39 A.M.


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_February2018Meeting_01102018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2018Meeting_01102018_postmeeting.pdf)
