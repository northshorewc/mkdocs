* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 12, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

### Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the August meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of current capital projects. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
7. Discussion and possible approval of the 2019 Operating Budget and the 2019 Capital Improvement Budget.
8. Discussion and possible action to approve 5 and 20 year capital improvement plans.
9. Status report of lead and copper monitoring.
10. Manager’s report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_September2018Meeting_09122018.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_October2018Meeting_09122018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_September2018Meeting_09122018_postmeeting.pdf)
