<!-- 2017-10-11-October2017Meeting.md -->
* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 14, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

## Agenda Items

1. Call to order by the Chairman.
* Presentation of the 2017 draft financial statements and other required communications by John Knepel of Baker Tilly. Possible action may be taken regarding this matter.
* Approval of the minutes of the January meeting.
* Monthly report of plant operations.
* Discussion of annual operating budget.
* Approval of monthly bills.
* Status report of current capital projects. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
* Discussion and possible approval to accept quotation for filter upgrade equipment and authorize procurement.
* Discussion and adoption of a Resolution of Commendation for Dave Eastman.
* Manager’s report.
* Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages rates and salaries of specified employees. The Commission may reconvene into open session to take action regarding this matter.
* Date and time of the next regular Commission Meeting.
* Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2018Meeting_02142018.pdf)

* * *
<!-- ## Minutes (*UNOFFICIAL*) -->
## Minutes
<!-- *Not available at this time.* -->

A meeting of the North Shore Water Commission was held at the Filtration Plant, 400 West Bender Road, Glendale, Wisconsin on Wednesday, February 14, 2018.

Meeting was called to order at 8:00 A.M. by Mr. Botcher.

Present:	Scott Botcher, Chair (Fox Point); Dave Eastman, Secretary (Glendale), Rachel Reiss, Alternate (Glendale); John Edlebeck, Member (Whitefish Bay)

Also present:	Eric Kiefer, Plant Manager & Recording Secretary; Charlie Imig; John Knepel, Auditor (Baker Tilly); Michelle Walter (Baker Tilly)

### FINANCIAL STATEMENTS

Mr. John Knepel and Ms. Michelle Walter from Baker Tilly presented the audited [2017 financial statements](../../financials/BakerTilly_report_FinancialStatements2017-Draft_February2018Meeting_February2018.pdf) and [other related communications](../../financials/BakerTilly_report_RequiredCommunicationsToCommission-Draft-2017Audit_March2018Meeting_March2018.pdf) to the Commission. Mr. Knepel noted there were no material weaknesses in internal controls and that Baker Tilly will be issuing an unmodified audit opinion of the financial statements.

Mr. Knepel reviewed various sections of the financial statements with the Commission. Pages 30, 31, 33 and 34 were discussed in great detail. In particular, he explained how the General Fund balance was determined as well as the Maintenance Reserve Fund (which will be renamed Capital Fund in 2018).

Mr. Edlebeck asked if the Commission had a policy regarding the maintenance of fund balance. Mr. Knepel and Mr. Kiefer explained that there is no policy; however, there will be a requirement to keep the General Fund balance at or above 12% of the operating budget starting in 2018 due to language in the Revised and Restated Agreement for Water Supply. However, the Capital Fund fund balance will not have any minimum or maximum requirement.

Mr. Knepel noted that page 29 of the report explains all of the significant changes that will occur in the near future due to the adoption of the new agreement.

Mr. Kiefer noted that he asked Mr. Knepel to set the target fund balance of $800,000 in the Maintenance Reserve Fund so that the upcoming reservoir project, estimated to cost $1,300,000 and scheduled for 2020, could be funded by fund balance. With that said, Mr. Kiefer acknowledged that the target Maintenance Reserve Fund is set at the discretion of the Commission.

Mr. Edlebeck suggested that the Commission consider adopting a policy for maintaining fund balance.

Mr. Botcher asked if it would be appropriate for the Commission to pay for the upcoming reservoir project with fund balance when the reservoir would be used by ratepayers over the next 40 or 50 years. Perhaps the member water utilities want to finance the upcoming project differently.

Mr. Edlebeck asked why the General Fund balance was less than 12% of the operating budget, and instead, members are receiving a refund. In response, Mr. Knepel indicated that 12% minimum does not take effect until 2018. Following past practice, Mr. Knepel assumed the Commission wanted to refund the surplus. Mr. Kiefer noted that it is likely that there will be a surplus next year and that money would be kept in General Fund to bring the fund balance to the minimum balance.

The Commission discussed whether the member communities are expecting a refund from the Commission. It was the consensus of the Commission that none of the members were expecting a refund.

Consequently, Mr. Botcher asked if all of the surplus fund balance could be transferred into the Maintenance Reserve Fund, after adjusting the General Fund balance to make 12% fund balance.

There was consensus from the Commission that should happen. Mr. Kiefer and Mr. Knepel mentioned, however, that there is a problem with doing that because the rate at which the funds come into the General Fund is different than the capital allocation rates. At some point, the an adjustment needs to be made so that funds are kept in the correct proportion by each member community.

Mr. Knepel and Mr. Botcher discussed technical aspects of how all of the money could be transferred as discussed without refunds going back to member communities. It was the consensus of the Commission to take no action regarding the financial statements until the Mr. Knepel and Mr. Kiefer make the requested changes to fund balance.

Before wrapping up, Mr. Knepel noted that the Commission should revise its OPEB study because of a new accounting standard. Mr. Botcher commented that revising the study should be relatively low cost.

No action was taken by the Commission regarding this issue.

Mr. Edlebeck made a request to Chair to take up agenda item 9. With no objections, Mr. Botcher suspended the rules and indicated that item 9 on the agenda would be taken up before advancing to item 3.

Mr. Knepel and Ms. Walter left the meeting at this time.

### RESOLUTION OF COMMENDATION

Mr. Kiefer explained that he and Mr. Botcher discussed Mr. Eastman’s departure. Mr. Botcher felt Mr. Eastman should be recognized for his outstanding service to the Commission and requested that a resolution of commendation be presented to Mr. Eastman.

The body of the resolution is shown below:

* * *

A RESOLUTION OF COMMENDATION

WHEREAS Dave Eastman has represented the City of Glendale as a member of the North Shore Water Commission since May 1, 2010, and

WHEREAS Dave Eastman served with diligence, industry, and fidelity in the many matters which came before the Commission, and

WHEREAS Dave Eastman was integral in making many significant improvements to the water treatment facilities including upgrades to chemical feed systems, rapid sand filters, and pumping equipment, and

WHEREAS Dave Eastman will be leaving the Commission after the February 14, 2018 meeting, and

NOW, THEREFORE, BE IT RESOLVED that the North Shore Water Commission, on behalf of its member municipalities and their residents, expresses its sincere thanks and gratitude to Dave Eastman for his many years of service as an active member of the Commission.

BE IT FURTHER RESOLVED that this resolution be made a permanent part of the Commission’s records, and that a copy be presented to Dave Eastman.

Dated and adopted this 14th day of February, 2018.

* * *

It was moved by Mr. Edlebeck, seconded by Mr. Botcher, and carried to adopt the [Resolution of Commendation](../../resolutions/NSWC_resolution_ResolutionofCommendation-DaveEastman_February2018Meeting_02142018_signed.pdf). Mr. Eastman abstained from voting.

### MINUTES

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried to approve the [minutes](../../minutes/NSWC_minutes_February2018Meeting_01102018_signed.pdf) for the meeting held January 10, 2018.

### MONTHLY REPORT OF PLANT OPERATIONS

Mr. Kiefer provided the Commission with a [report](../../monthly-reports/NSWC_report_MonthlyReportOfOperations_February2018Meeting_02082018.pdf) regarding plant operations. The report was placed on file without any motion.

### ANNUAL OPERATING BUDGET

Mr. Kiefer presented the [monthly financial reports](../../financials/NSWC_financials_February2018Meeting_02092018.pdf), and they were put on file without motion.

### MONTHLY BILLS

It was moved by Mr. Eastman, seconded by Mr. Edlebeck, and unanimously carried that the following payments be approved and authorization was given to the Fiscal Agent to make such payments:

| Vendor                                                                                                         |     Amount |
|:---------------------------------------------------------------------------------------------------------------|-----------:|
| Alexander Chemical (treatment chemical: aluminum sulfate)                                                      |  $2,449.70 |
| American Bolt (hardware)                                                                                       |     $38.52 |
| BMO Harris Bank (credit card - Operating Fund)                                                                 |    $500.61 |
| -- AT&T (monthly charge for internet and one-time installation fees): $58.99                                   |            |
| -- Delta Adsorbents (activated alumina for air dryer): $292.56                                                 |            |
| -- Google (monthly charge for Google Suite Basic): $54.16                                                      |            |
| -- Straight Talk (monthly cell phone charges): $45.90                                                          |            |
| -- USPS (postage): $49.00                                                                                      |            |
| Diversified Benefit Services (Section 125 Plan administration 2017 and enrollment fees)                        |    $199.50 |
| Eurofins (compliance monitoring)                                                                               |    $380.00 |
| Grainger (plumbing materials, grease, oil, hardware, center pull towel, sensors, conductivity standard, fuses) |    $499.12 |
| Great America (lease payment for copier/printer & property tax fee)                                            |    $195.85 |
| Hach (lamp for DR5000)                                                                                         |    $240.27 |
| Hawkins (treatment chemicals: aqueous ammonia and phosphates)                                                  |  $2,459.81 |
| Home Depot (concrete)                                                                                          |    $133.25 |
| Idexx (colilert, quantitrays, and collection vessels)                                                          |  $2,841.10 |
| John Mahnke (reimbursement for online classes)                                                                 |     $60.00 |
| Lakeland Chemical Specialties (boiler treatment chemicals)                                                     |    $229.92 |
| Lubrication Engineers (grease)                                                                                 |     $93.20 |
| Minnesota Life / Securian (employee life insurance)                                                            |    $128.92 |
| Mulcahy Shaw Water (chlorine analyzer reagents)                                                                |    $454.50 |
| Northern Lake Service (compliance monitoring)                                                                  |     $68.00 |
| Office Copying Equipment (maintenance payment for copier/printer)                                              |     $23.70 |
| Olin (treatment chemical: sodium hypochlorite)                                                                 |  $2,723.67 |
| Rotroff Jeanson (monthly accounting services, audit assistance, and postage)                                   |  $2,065.05 |
| Spectrum Business (internet services, Bender phone, and Klode phone)                                           |    $521.11 |
| Starnet Technologies (control system support contract and quarterly data charge)                               | $12,689.00 |
| Superior Chemical (floor cleaner and wipes)                                                                    |    $169.05 |
| T-Mobile (mobile internet)                                                                                     |     $31.05 |
| US Cellular (cellular phone service)                                                                           |      $4.25 |
| USA Bluebook (chemical transfer pump repair parts and phosphate reagents)                                      |  $1,570.14 |
| Village Ace (hardware)                                                                                         |     $44.96 |
| Village of Fox Point (gasoline)                                                                                |     $57.12 |
| Wallace Tree & Landscape (snow removal)                                                                        |    $700.00 |
| We Energies (Bender Electric)                                                                                  | $13,770.06 |
| We Energies (Bender Gas)                                                                                       |  $2,057.07 |
| We Energies (Green Tree Electric)                                                                              |     $17.07 |
| We Energies (Henry Clay Electric)                                                                              |     $16.91 |
| We Energies (Klode Electric)                                                                                   |  $3,920.60 |
| We Energies (Klode Gas)                                                                                        |     $45.70 |
| Wilkens-Anderson (plate count agar)                                                                            |     $94.97 |
| Wisconsin State Lab of Hygiene - ESTIMATE (fluoride analysis)                                                  |     $25.00 |
|                                                                                                                |            |
| **SUB-TOTAL**                                                                                                  | $51,518.75 |
| *Maintenance Reserve*                                                                                          |            |
|                                                                                                                |            |
| **SUB-TOTAL**                                                                                                  |      $0.00 |
|                                                                                                                |            |
| **TOTAL**                                                                                                      | $51,518.75 |

### CAPITAL PROJECTS

Mr. Kiefer provided a brief report about the capital improvement projects. He reported that some materials were received for the filter improvements project and several hours of labor were used to set up pressure transducers.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to approve the [payment request](../../payment-requests/NSWC_paymentRequests_February2018Meeting_02142018.pdf) in the amount of $3,162.99 as per Mr. Kiefer’s memo dated February 14, 2018.

### FILTER UPGRADE EQUIPMENT

Mr. Kiefer provided the Commission with a [tabulation](../../tabulations/NSWC_report_Tabulations_February2018Meeting_02132018.pdf) of quotations provided for Limitorque electric valve actuators, pressure transducers, and valve manifolds. Mr. Kiefer recommended that the Commission authorize the procurement of equipment from the lowest cost vendors as shown in his memos.

It was moved by Mr. Edlebeck, seconded by Mr. Eastman, and unanimously carried to approve the procurement of the valve actuators from [Swanson Flo](../../proposals/SwansonFlo_quotation_FilterUpgradeEquipment_February2018Meeting_02062018.pdf) in the amount of $47,6000 and other pressure transmitter equipment from [Industrial Controls](../../proposals/IndustrialControls_quotation_TransmitterQuotation_February2018Meeting_02132018.pdf) in the amount of $23,595.00. Funding to come from member communities using the capital allocation rates.

### MANAGER’S REPORT

1. Plant staff replaced the motor for sludge pump #1 which has not been operational for several years. After restoring operating to sludge pump #1, pump #2 was taken out of service for maintenance.
* Plant staff inspected basin chains and found 6 chains were broken. These chains are scheduled to be repaired this summer.
* Plant staff conducted an annual test of its reference magmeter and determined its accuracy and precision has not changed since last year.
* On January 22, there was a brownout at Bender and a 4-hour power outage at Klode Park. During that time, emergency generators were used.
* On February 12, plant staff experienced an unusual network switch failure. Network functions have been temporarily restored. New equipment is scheduled to arrive on February 14 so that permanent fixes can be made.

Member John Edlebeck left the meeting at 9:00 AM.

### CLOSED SESSION

At 9:00 A.M. it was moved by Mr. Eastman, seconded by Mr. Botcher, and unanimously carried that the Commission enter into closed session pursuant to section 19.85 (c) of Wisconsin Statutes to discuss and consider wage rates and salaries for specified employees.

By roll call vote, the Commission reconvened into open session at 9:15 A.M.

It was moved by Mr. Eastman, seconded by Mr. Botcher, and unanimously carried to approved the compensation plan for its hourly employees as described in Mr. Kiefer’s [memo](../../memos/NSWC_memo_2018Wages_February2018Meeting_02142018.pdf) dated 2/14/2018. Furthermore, the Commission is increasing Mr. Kiefer’s annual 2018 salary from $88,900 to $91,567 and an increase in his vacation allowance by 5 days to reflect the Commission’s satisfaction with his performance as Plant Manager.

### NEXT MEETING

The next regular meeting was scheduled for Wednesday, March 14, 2018 at 8:00 AM.

### ADJOURNMENT

It was moved by Mr. Eastman, seconded by Mr. Botcher, and unanimously carried to adjourn at 9:17 A.M.

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2018Meeting_02142018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2018Meeting_02142018_postmeeting.pdf)
