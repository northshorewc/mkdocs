* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 12, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the November meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the SCADA Upgrade Project and the Filter Improvements Project. Action may be taken to approve capital expenditures pertaining to these projects and authorize payment to vendors.
7. Discussion and possible approval to purchase software from Inductive Automation for the SCADA upgrade project.
8. Discussion of recent problems with high service pump #4 and possible approval to give Plant Manager authorization to make and pay for any necessary repairs.
9. Discussion of the upcoming 2019 capital improvement project to design and develop bid documents for the 2020 reservoir improvement project. Possible action may be taken regarding this matter.
10. Discussion of key large diameter mains in the distribution system, techniques used for locating mains, and process that can be used to respond to locate requests. Possible action may be taken regarding this matter including the purchase of locating equipment.
11. Discussion and possible acceptance of report prepared by Key Benefit Concepts regarding other post-employment benefits.
12. Manager’s report.
13. Date and time of the next regular Commission Meeting.
14. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2018Meeting_12122018.pdf)

* * *
## Minutes

*Not available at this time.*
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2019Meeting_12122018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2018Meeting_12122018_postmeeting.pdf)
