* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **June 13, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

### Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the May meeting.
3. Monthly report of plant operations.
4. Discussion of annual operating budget.
5. Approval of monthly bills.
6. Status report of current capital projects. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
7. Discussion of proposals for replacing the roof and siding of the garage. Action may be taken to accept a proposal.
8. Discussion of lead and copper testing scheduled for this summer and the latest update from the Department of Natural Resources.
9. Discussion of a draft proposal from Clark Dietz regarding a phosphate optimization study.
10. Manager’s report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_June2018Meeting_06132018.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_July2018Meeting_06132018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_June2018Meeting_06132018_postmeeting.pdf)
