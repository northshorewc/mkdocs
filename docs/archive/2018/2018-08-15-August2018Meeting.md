* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 15, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

### Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the July meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of current capital projects. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
7. Discussion and possible action to authorize renewal of causality and property insurance policies.
8. Discussion and possible action to purchase new motor for high service pump #4 from CTW Corporation.
9. Discussion and possible action to purchase new electric trash pump.
10. Review of engineering proposals to assist the Commission in switching corrosion control treatment from blended phosphates to phosphoric acid and possible action to select proposal.
11. Discussion of the preliminary 2019 Operating Budget and 2019 Capital Improvement Budget.
12. Status report of lead and copper monitoring.
13. Manager’s report.
14. Date and time of the next regular Commission Meeting.
15. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2018Meeting_08152018.pdf)

* * *
## Minutes

*Not available at this time.*
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2018Meeting_08152018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2018Meeting_08152018_postmeeting.pdf)
