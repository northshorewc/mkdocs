* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 10, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

### Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the September meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of current capital projects. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
7. Update regarding the implementation of the Revised and Restated Agreement for Water Supply System and the current accounting system. Possible action may be taken to make changes to accounting system.
8. Discussion and approval to authorize Fiscal Agent to transfer funds between accounts.
9. Discussion of the benefits and costs associated with becoming a member of Diggers Hotline. Possible action may be taken to initiate membership process.
10. Manager’s report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2018Meeting_10102018.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2018Meeting_10102018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2018Meeting_10102018_postmeeting.pdf)
