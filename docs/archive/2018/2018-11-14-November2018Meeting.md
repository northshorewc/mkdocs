* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 14, 2018** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

### Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the October meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the SCADA Upgrade Project and the Filter Improvements Project. Action may be taken to approve capital expenditures pertaining to the projects and authorize payment to vendors.
7. Discussion and possible action to accept chemical quotations for 2019.
8. Discussion of large diameter mains in the distribution system and the possibility of becoming a member of Diggers Hotline. Possible action may be taken regarding this matter.
9. Update regarding lead and copper monitoring results and WDNR’s response to proposed change in corrosion control chemical. Possible action may be taken regarding this matter.
10. Manager’s report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_November2018Meeting_11142018.pdf)

* * *
## Minutes

*Not available at this time.*
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_December2018Meeting_11142018_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_November2018Meeting_11142018_postmeeting.pdf)
