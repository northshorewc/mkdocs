* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 15, 2025** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 8:40 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes for the December meeting.  
3. Discussion of a metering problem at the Henry Clay interconnection and possible action to correct member delivery totals from May 22, 2024 through December 31, 2024 and the corresponding reports.  
4. Monthly report of plant operations.  
5. Discussion of annual budget.  
6. Review of payment reports and approval of payment requests for capital expenditures.  
7. Update regarding the progress of capital projects.  
8. Discussion of new capital allocation rates calculated by plant staff pursuant to Article 8.15.1 of the Revised and Restated Agreement for Water Supply System. Possible action may be taken by the Commission to accept the rates.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2025Meeting_01152025.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_February2025Meeting_01152025_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2025Meeting_01152025_postmeeting.pdf)
