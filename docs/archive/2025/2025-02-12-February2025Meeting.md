* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 12, 2025** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:36 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the January meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Review of payment reports and approval of payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion of BASF and Tyco PFAS class action lawsuits and a strategy for filing claims. Possible action may be taken regarding this matter.  
8. Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages rates and salaries of employees. The Commission may reconvene into open session to take action regarding this matter.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2025Meeting_02122025.pdf)

* * *
## Minutes

*Not available at this time.*

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2025Meeting_02122025_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2025Meting_02122025_postmeeting.pdf)
