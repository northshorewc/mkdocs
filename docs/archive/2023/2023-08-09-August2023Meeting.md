* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 9, 2023** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:45 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the July meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills and payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion and possible action to select proposal for engineering services pertaining to the pre-design of basin #5 renovations.  
8. Discussion and possible action to authorize renewal of causality and property insurance policies.  
9. Discussion of the preliminary 2024 operating and capital improvement budgets and the anticipated capital allocation rates for 2024.  
10. Date and time of the next regular Commission Meeting.  
11. Adjournment.
 

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2023Meeting_08092023.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2023Meeting_08092023_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2023Meeting_08092023_postmeeting.pdf)
