* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 8, 2023** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:18 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the January meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of proposals received for valve actuator replacements. Possible action may be taken to accept a proposal.  
7. Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages rates and salaries of specified employees. The Commission may reconvene into open session to take action regarding this matter.  
8. Date and time of the next regular Commission Meeting.  
9. Adjournment.   

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2023Meeting_02082023.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2023Meeting_02082023_signed.pdf)    

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2023Meeting_02082023_postmeeting.pdf)
