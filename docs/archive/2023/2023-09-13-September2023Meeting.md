* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 13, 2023** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:36 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the August meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills and payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion and possible action regarding PFAS MDL settlements.  
8. Discussion and possible adoption of the 2024 operating and capital improvement budgets.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.  
 

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_September2023Meeting_09132023.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_October2023Meeting_09132023_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_September2023Meeting_09132023_postmeeting.pdf)
