* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 23, 2023** using an internet-based online conferencing platform called Zoom. The meeting started at 9:00 AM and ended at 9:18 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Discussion and possible action to approve a construction permit to grant the State of Wisconsin, Department of Transportation a temporary right to occupy and use land at the Commission’s main facility.  
3. Adjournment.
 
[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February232023Meeting_02232023.pdf)


* * *
## Minutes

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2023Meeting_02232023_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February232023Meeting_02232023_postmeeting.pdf)
