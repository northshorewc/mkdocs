* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 11, 2023** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:41 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Report by Baxter & Woodman regarding the condition assessment of Basin #5 and progress made with the pre-design of Basin #5 improvements.  
3. Report by staff regarding basin and filter performance.  
4. Approval of the minutes of the September meeting.  
5. Monthly report of plant operations.  
6. Discussion of annual budget.  
7. Approval of monthly bills and payment requests for capital expenditures.  
8. Update regarding the progress of capital projects.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.  
 

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2023Meeting_10112023.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2023Meeting_11082023_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2023Meeting_10112023_postmeeting.pdf)
