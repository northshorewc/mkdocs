* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 11, 2023** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:50 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Discussion of budgeted capital projects for 2023 and latest sanitary survey report from Wisconsin Department of Natural Resources. Possible action to proceed with recommendations proposed by Baxter & Woodman and to determine a course of action to satisfy regulatory requirements mentioned in the sanitary survey report.  
3. Approval of the minutes of the December meeting.  
4. Monthly report of plant operations.  
5. Discussion of annual budget.  
6. Approval of monthly bills.  
7. Discussion of the Filter Upgrade, Sludge Equipment Improvement, Filter Railing Repairs and Column Stabilization, and Breaker Replacement Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.
8. Discussion of new capital allocation rates calculated by plant staff pursuant to Article 8.15.1 of the Revised and Restated Agreement for Water Supply System. Possible action may be taken by the Commission to accept the rates.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment. 

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2023Meeting_01112023.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_February2023Meeting_01112023_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2023Meeting_01112023_postmeeting.pdf)
