* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 13, 2023** at the Water Filtration Plant with the ability for attendence using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:14 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Report by Baxter & Woodman regarding the pre-design of Basin #5 improvements.  
3. Approval of the minutes of the October meeting.  
4. Monthly report of plant operations.  
5. Discussion of annual budget.  
6. Approval of monthly bills and payment requests for capital expenditures.  
7. Update regarding the progress of capital projects.  
8. Discussion and possible action regarding the process in which the Plant Manager will use to solicit proposals for a generator and associated equipment that are planned to be installed in 2024.  
9. Discussion and possible action to accept chemical quotations for 2024.  
10. Date and time of the next regular Commission Meeting.  
11. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2023Meeting_12132023.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2024Meeting_12132023_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2023Meeting_12132023_postmeeeting.pdf)
