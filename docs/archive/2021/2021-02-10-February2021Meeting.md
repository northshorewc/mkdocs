* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 10, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:39 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Presentation of the 2020 draft financial statements and other required communications by Baker Tilly. Possible action may be taken regarding this matter.
3. Approval of the minutes of the January meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Approval of monthly bills.
7. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
8. Discussion of the IT Improvements Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
9. Discussion of the Section 125 Plan and possible changes. Action may be taken to approve changes to the plan.
10. Discussion of current lead and copper corrosion control program and recent changes to regulations. Possible action may be taken by the Commission regarding this matter.
11. Discussion of the upcoming rotation of the Fiscal Agent office.
12. Presentation of monthly operations and maintenance report.
13. Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages rates and salaries of specified employees. The Commission may reconvene into open session to take action regarding this matter.
14. Date and time of the next regular Commission Meeting.
15. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2021Meeting_02102021.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2021Meeting_02102021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2021Meeting_02102021_postmeeting.pdf)
