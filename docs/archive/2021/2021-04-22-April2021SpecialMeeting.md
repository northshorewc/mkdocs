* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **April 22, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 9:00 AM and ended at 10:21 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Pursuant to section 19.85 (1) (E) Wisconsin statutes, this Commission reserves the right to convene in closed session to deliberate and discuss estimated fees, specific strategies, and ask questions associated with the Statements of Qualifications received for the Demonstrative Corrosion Control Study and Compliance Assistance Project. Competitive and bargaining reasons require closed session discussion. This Commission may reconvene in open session to take action relating to these issues.
3. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_Special_04222021.pdf)


* * *
## Minutes

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_May2021Meeting_04222021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_SpecialMeeting04222021_postmeeting.pdf)
