* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 13, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:43 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the meetings held on 12/9/2020 and 12/16/2020.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of new capital allocation rates calculated by plant staff pursuant to Article 8.15.1 of the Revised and Restated Agreement for Water Supply System. Possible action may be taken by the Commission to accept the rates.
7. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
8. Discussion of current lead and copper corrosion control program and recent changes to regulations. Possible action may be taken by the Commission regarding this matter.
9. Discussion of the upcoming rotation of the Fiscal Agent office.
10. Presentation of monthly operations and maintenance report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2021Meeting_01132021.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_Minutes_February2021Meeting_01132021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2021Meeting_01132021_postmeeting.pdf)
