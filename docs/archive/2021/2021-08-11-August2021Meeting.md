* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 11, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 8:59 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the July meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Reservoir Upgrade, Filter Upgrade, Video Surveillance, and IT Improvements Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion and possible action to authorize renewal of causality and property insurance policies.  
8. Discussion of report from McEnroe Consulting Engineers regarding the filter room railing and columns.  
9. Discussion of possible solar panel installation at the water filtration plant.  
10. Discussion of the preliminary 2021 operating and capital improvement budgets and the anticipated capital allocation rates for 2021.  
11. Update regarding the progress of the demonstrative corrosion control study and discussion of upcoming tasks associated with the study that requires member utility participation.  
12. Discussion and possible action to accept a proposal for a firm to provide lab services associated with the demonstrative corrosion control study.  
13. Date and time of the next regular Commission Meeting.  
14. Adjournment.  


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2021Meeting_08112021.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2021Meeting_08112021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2021Meeting_08112021_postmeeting.pdf)
