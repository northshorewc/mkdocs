* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 13, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:48 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the September meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Reservoir Upgrade, Filter Upgrade, Video Surveillance, and IT Improvements Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion of the Mutual Assistance Agreement that was signed in 2003. Possible action may be taken to address the lack of a date on the document.  
8. Discussion and possible action regarding what the Commission should do with proceeds from the sale of property to Wisconsin DOT.
9. Discussion and possible action regarding the installation of solar panels.  
10. Discussion and possible approval of capital improvements plan.  
11. Date and time of the next regular Commission Meeting.  
12. Adjournment.  


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2021Meeting_10132021.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2021Meeting_Amended_10132021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2021Meeting_10132021_postmeeting.pdf)
