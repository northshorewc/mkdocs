* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 8, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 8:20 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the November meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Reservoir Upgrade, Filter Upgrade, Video Surveillance, and IT Improvements Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Corrosion control study update.  
8. Date and time of the next regular Commission Meeting.  
9. Adjournment.  


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2021Meeting_12082021.pdf)


* * *
## Minutes

*Not available at this time.*


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2022Meeting_12082021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2021Meeting_12082021_postmeeting.pdf)
