* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 8, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:02 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the August meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Reservoir Upgrade, Filter Upgrade, Video Surveillance, and IT Improvements Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion and possible adoption of the 2021 operating and capital improvement budgets.  
8. Update regarding the progress of the demonstrative corrosion control study and discussion of upcoming tasks associated with the study that requires member utility participation.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.  


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_September2021Meeting_09082021.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_October2021Meeting_09082021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_September2021Meeting_09082021_postmeeting.pdf)
