* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 10, 2021** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:07 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the October meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Reservoir Upgrade, Filter Upgrade, Video Surveillance, and IT Improvements Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion and possible action to accept chemical quotations for 2022.  
8. Discussion and possible approval of capital improvements plan.  
9. Pursuant to section 19.85 (1) (E) Wisconsin statutes, this Commission reserves the right to convene in closed session for competitive bargaining reasons to discuss strategy for retaining a consultant to assist in the conversion of secondary disinfectant from chloramine to chlorine. This Commission will reconvene in open session to discuss the Plant Manager’s recommendation for changing the secondary disinfectant from chloramine to chlorine and may take action related to this matter.  
10. Date and time of the next regular Commission Meeting.  
11. Adjournment.  


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_November2021Meeting_11102021.pdf)


* * *
## Minutes



[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_December2021Meeting_11102021_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_November2021Meeting_11102021_postmeeting.pdf)
