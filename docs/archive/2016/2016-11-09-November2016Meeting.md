* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 9, 2016** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_November2016Meeting_11092016.pdf)

[Minutes](../../minutes/NSWC_minutes_December2016Meeting_11092016_signed.pdf)

[Packet](../../materials/NSWC_packet_November2016Meeting_11092016_postmeeting.pdf)
