* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 14, 2016** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_September2016Meeting_09142016.pdf)

[Minutes](../../minutes/NSWC_minutes_October2016Meeting_09142016_signed.pdf)

[Packet](../../materials/NSWC_packet_September2016Meeting_09142016_postmeeting.pdf)
