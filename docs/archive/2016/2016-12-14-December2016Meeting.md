* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 14, 2016** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_December2016Meeting_12142016.pdf)

[Minutes](../../minutes/NSWC_minutes_January2017Meeting_12142016_signed.pdf)

[Packet](../../materials/NSWC_packet_December2016Meeting_12142016_postmeeting.pdf)
