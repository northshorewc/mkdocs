* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 12, 2016** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *

[Agenda](../../agenda/NSWC_Agenda_October2016Meeting_10122016.pdf)

[Minutes](../../minutes/NSWC_minutes_November2016Meeting_10122016_signed.pdf)

[Packet](../../materials/NSWC_packet_October2016Meeting_10122016_postmeeting.pdf)
