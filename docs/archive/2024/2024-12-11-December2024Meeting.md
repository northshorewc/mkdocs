* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 11, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:39 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes for the November meetings.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Review of payment reports and approval of payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion and possible action to accept a revised quotation for orthophosphate in 2025.  
8. Date and time of the next regular Commission Meeting.  
9. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2024Meeting_12112024.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2025Meeting_12112024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2024Meeting_12112024_postmeeting.pdf)
