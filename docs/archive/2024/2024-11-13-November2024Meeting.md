* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 13, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:49 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes for the October meetings.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Review of payment reports and approval of payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion and possible action to accept chemical quotations for 2025.  
8. Discussion of 2025 lead and copper testing.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment. 

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_November2024Meeting_11132024.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_December2024Meeting_11132024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_November2024Meeting_11132024_postmeeting.pdf)
