* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 14, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:59 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the January meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills and payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion and possible approval of resolution to replace Nicole Maurer and Kelly Meyer as Fiscal Agents with Jialin Li and Sara Bruckman for the Commission’s account at WaterStone Bank.  
8. Discussion and possible approval of capital improvement plan.  
9. Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages rates and salaries of employees. The Commission may reconvene into open session to take action regarding this matter.  
10. Date and time of the next regular Commission Meeting.  
11. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2024Meeting_02142024.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2024Meeting_02142024_signed.pdf)


* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2023Meeting_02142024_postmeeting.pdf)
