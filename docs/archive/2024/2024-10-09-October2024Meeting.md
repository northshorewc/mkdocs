* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 9, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:42 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Discussion of Public Service Commission’s requirement regarding construction authorization and the possibility of retaining SEH and City Water to assist with the application process.
3. Approval of the minutes of the September meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Review of payment reports and approval of payment requests for capital expenditures.
7. Update regarding the progress of capital projects.
8.  Discussion and possible approval to authorize the Plant Manager to sell the existing Klode generator on behalf of the member utilities, with the proceeds going to the Capital Fund.
9. Date and time of the next regular Commission Meeting.
10. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2024Meeting_10092024.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2024Meeting_10092024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2024Meeting_10092024_postmeeting.pdf)
