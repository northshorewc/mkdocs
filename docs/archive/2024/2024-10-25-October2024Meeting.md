* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 25, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:18 AM.

* * *
## Agenda

1. Call to order by the Chair. 
2. Discussion of Public Service Commission’s requirement regarding construction authorization. Possible action may be taken to retain SEH and/or City Water to assist with the application process.  
3. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2024Meeting_10252024.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2024Meeting_10252024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2024Meeting_10252024_postmeeting.pdf)
