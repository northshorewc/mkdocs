* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 10, 2024** at the Water Filtration Plant with the ability for attendence using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:49 AM.

* * *
## Agenda

1. Call to order by the Chair.   
2. Report by Baxter & Woodman regarding the pre-design of Basin #5 repairs and the possible use of the Safe Drinking Water Loan Program (SDWLP) to finance the project. Possible action may be taken by the Commission regarding this matter.  
3. Discussion regarding Basin #5 Repair Project and possible approval to solicit engineering proposals.  
4. Approval of the minutes of the December meeting.  
5. Monthly report of plant operations.  
6. Discussion of annual budget.  
7. Approval of monthly bills and payment requests for capital expenditures.  
8. Update regarding the progress of capital projects.  
9. Discussion of a memorandum of understanding regarding emergency water supply by member communities to the Village of Shorewood.  
10. Discussion of new capital allocation rates calculated by plant staff pursuant to Article 8.15.1 of the Revised and Restated Agreement for Water Supply System. Possible action may be taken by the Commission to accept the rates.  
11. Date and time of the next regular Commission Meeting.
12. Adjournment. 


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2024Meeting_01102024.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_February2024Meeting_01102024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2024Meeting_01102024_postmeeting.pdf)
