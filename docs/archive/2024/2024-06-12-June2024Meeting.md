* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **June 12, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:35 AM.

* * *
## Agendas

1. Call to order by the Chair.   
2. Approval of the minutes of the May meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills and payment requests for capital expenditures.  
6. Update regarding the progress of capital projects.  
7. Discussion of a problem with a critical 42” PCCP main connecting the reservoirs to the high services pumps. Possible action may be taken to authorize a proposal to repair the main.  
8. Discussion of the current plans for the Klode Generator Replacement and Roof Replacement at Klode Park. Possible action may be taken regarding this matter.  
9. Discussion of the expiring engagement with Baker Tilly and a proposal for a new engagement. Possible action may be taken regarding this matter.  
10. Discussion of potential changes to the process used for authorizing payment to vendors and the related internal controls.  
11. Discussion regarding a general service agreement with SEH to provide engineering services as needed by the Plant Manager. Assistance with the capital improvement plan, if any, would be provided using this contract.  
12. Date and time of the next regular Commission Meeting.  
13. Adjournment.  

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_June2024Meeting_06122024.pdf)

* * *
## Minutes

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_July2024Meeting_06122024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_June2024Meeting_06122024_postmeeting.pdf)
