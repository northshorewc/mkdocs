* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 14, 2024** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:53 AM.

* * *
## Agenda

1. Call to order by the Chair.   
2. Presentation by R&R Insurance and possible action to authorize the renewal of causality and property insurance policies.  
3. Discussion of bids received for projects 2024-2 and 2024-3 which correspond to the Klode Generator Replacement project and the Klode Roof Replacement projects. Action may be taken to accept a bid.  
4. Approval of the minutes of the July meeting.  
5. Monthly report of plant operations.  
6. Discussion of annual budget.  
7. Review of check disbursements and approval of payment requests for capital expenditures.  
8. Update regarding the progress of capital projects.  
9. Discussion of the preliminary 2025 operating and capital improvement budgets and the anticipated capital allocation rates for 2025.  
10. Date and time of the next regular Commission Meeting.
11. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2024Meeting_08142024.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2024Meeting_08142024_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2024Meeting_08132024_postmeeting.pdf)
