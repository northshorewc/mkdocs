* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 12, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 8:50 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Discussion and possible approval of a plan to retain A. C. Engineering to rebuild 3 electrical breakers as part of capital project 2022-5.  
3. Discussion of PFAS levels in Lake Michigan, health advisory levels, and upcoming monitoring required by the EPA and WDNR.  
4. Approval of the minutes of the September meeting.  
5. Monthly report of plant operations.  
6. Discussion of annual budget.  
7. Approval of monthly bills.  
8. Discussion of the Filter Upgrade, Sludge Equipment Improvement, Bender and Klode Door Replacements, and Filter Railing Repairs and Column Stabilization Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.   

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2022Meeting_10122022.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2022Meeting_10122022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2022Meeting_10122022_postmeeting.pdf)
