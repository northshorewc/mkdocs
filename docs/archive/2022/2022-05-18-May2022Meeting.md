* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **May 18, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:02 AM and ended at 9:13 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Recognition of the annual rotation of office on May 1: Whitefish Bay, Chair; Fox Point, Secretary; Glendale, Member & Fiscal Agent.   
3. Approval of the minutes of the April meeting.  
4. Monthly report of plant operations.  
5. Discussion of annual budget.  
6. Approval of monthly bills.  
7. Discussion of the Filter Upgrade and Sludge Equipment Improvement Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
8. Discussion and possible approval to accept quotations for valve actuators related to the Filter Upgrade and Sludge Equipment Improvements Projects and to authorize procurement.  
9. Discussion of the latest employee to separate employment with the Commission and proposed changes to job descriptions, employee handbook, organizational chart, and other related topics. Action may be taken to adopt a resolution regarding this matter.   
10. Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages and benefits of specified employees. The Commission may reconvene into open session to take action regarding this matter.  
11. Date and time of the next regular Commission Meeting.  
12. Adjournment.  


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_May2022Meeting_05182022.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_June2022Meeting_05182022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_May2022Meeting_05182022_postmeeting.pdf)
