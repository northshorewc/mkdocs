* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 10, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 9:13 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Presentation of proposals by R&R Insurance. Discussion and possible action to authorize renewal of causality and property insurance policies and to purchase additional cyber insurance.  
3. Presentation of findings and recommendations of the demonstrative corrosion control study by Baxter & Woodman. Discussion and possible action may be taken regarding this matter.  
4. Approval of the minutes of the July meeting.  
5. Monthly report of plant operations.  
6. Discussion of annual budget.  
7. Approval of monthly bills.  
8. Discussion of the Filter Upgrade, Sludge Equipment Improvement, Bender and Klode Door Replacements, and Filter Railing Repairs and Column Stabilization Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
9. Discussion and possible approval to accept a proposal related to the Sludge Equipment Improvements Projects and to authorize procurement.  
10. Discussion of the preliminary 2023 operating and capital improvement budgets and the anticipated capital allocation rates for 2023.  
11. Date and time of the next regular Commission Meeting.  
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2022Meeting_08102022.pdf)


* * *
## Minutes



[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2022Meeting_08102022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2022Meeting_08102022_postmeeting.pdf)
