* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 9, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 9:00 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the October meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Filter Upgrade, Sludge Equipment Improvement, Bender and Klode Door Replacements, and Filter Railing Repairs and Column Stabilization Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.
7. Discussion and possible approval of a plan to retain A. C. Engineering to rebuild 3 electrical breakers or to purchase hardware from A. C. Engineering as part of capital project 2022-5.
8. Discussion and possible action to accept chemical quotations for 2023.
9. Discussion and possible action to select quotations for lab services in 2023.
10. Discussion of ongoing complaints from residents about home reverse osmosis filters that are prematurely failing and plans by staff to investigate the matter.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_November2022Meeting_11092022.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_December2022Meeting_11092022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_November2022Meeting_11092022_postmeeting.pdf)
