* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **June 15, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 8:41 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the May meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Filter Upgrade and Sludge Equipment Improvement Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion and possible approval to accept quotations for door replacements at Bender and Klode facilities and to authorize procurement.  
8. Discussion about the priority of upcoming capital projects including the improvements needed to go to unattended operations.  
9. Discussion of cyber liability insurance, the feasibility of obtaining said coverage, and general strategy for reducing risks and liability associated with information technology (IT) and operational technology (OT).  
10. Date and time of the next regular Commission Meeting.  
11. Adjournment.  

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_June2022Meeting_06152022.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_July2022Meeting_06152022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_June2022Meeting_06152022_postmeeting.pdf)
