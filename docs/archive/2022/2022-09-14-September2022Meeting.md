* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 14, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 9:13 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the August meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Filter Upgrade, Sludge Equipment Improvement, Bender and Klode Door Replacements, and Filter Railing Repairs and Column Stabilization Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion and possible acceptance of a report prepared by Key Benefit Concepts regarding post-employment benefits.  
8. Discussion of PFAS Cost Recovery Program advertised by Wisconsin Rural Water Association and the law firm of Napoli Shkolnik. Possible action may be taken by the Commission.  
9. Discussion and possible adoption of the 2023 operating and capital improvement budgets.  
10. Date and time of the next regular Commission Meeting.  
11. Adjournment.  

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_September2022Meeting_09142022.pdf)


* * *
## Minutes



[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_October2022Meeting_09142022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_September2022Meeting_09142022_postmeeting.pdf)
