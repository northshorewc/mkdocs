* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 14, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:40 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the November meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Filter Upgrade, Sludge Equipment Improvement, Bender and Klode Door Replacements, Filter Railing Repairs and Column Stabilization, and Breaker Replacement Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion of anticipated 2023 capital improvements.  
8. Discussion and possible approval of capital improvements plan.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.  

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2022Meeting_12142022.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2023Meeting_12142022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2022Meeting_12142022_postmeeting.pdf)
