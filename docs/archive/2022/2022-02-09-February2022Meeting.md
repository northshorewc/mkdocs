* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 9, 2022** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:37 AM.

* * *
## Agenda

1. Call to order by the Chair.  
2. Approval of the minutes of the December meeting.  
3. Monthly report of plant operations.  
4. Discussion of annual budget.  
5. Approval of monthly bills.  
6. Discussion of the Filter Upgrade, Video Surveillance, and IT Improvements Projects. Action may be taken to approve capital expenditures pertaining to these projects and to authorize payment to vendors.  
7. Discussion of new capital allocation rates calculated by plant staff pursuant to Article 8.15.1 of the Revised and Restated Agreement for Water Supply System. Possible action may be taken by the Commission to accept the rates.  
8. Update regarding the possible change of secondary disinfectant.  
9. Date and time of the next regular Commission Meeting.  
10. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2022Meeting_02092022.pdf)


* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2022Meeting_02092022_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2022Meeting_02092022_postmeeting.pdf)
