* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 8, 2020** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the December meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the Reservoir Upgrade Project with SEH representative via conference call. Discussion may include topics such as project administration, work schedule, quality of work products, communications, customer service, and plan for meeting compliance deadline. Possible action may be taken regarding these issues.
7. Discussion of new capital allocation rates calculated by plant staff pursuant to Article 8.15.1 of the Revised and Restated Agreement for Water Supply System. Possible action may be taken by the Commission to accept the rates.
8. Discussion of invoices received for work performed on the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and authorize payment to vendors.
9. Discussion of proposed organizational changes, employee handbook changes, and related topics. Action may be taken to adopt a resolution regarding changes to staff organization, job descriptions, policies, and compensation.
10. Discussion of lead and copper corrosion monitoring. Possible action may be taken to authorize the Plant Manager to initiate water quality changes with the intent of reducing lead corrosion. Action may be taken to authorize Plant Manager to sign a new agreement with Process Research Solutions.
11. Manager’s report.
12. Date and time of the next regular Commission Meeting.
13. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2020Meeting_01082020.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_February2020Meeting_01082020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2020Meeting_01082020_postmeeting.pdf)
