* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 12, 2020** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Presentation of the 2019 draft financial statements and other required communications by Baker Tilly. Possible action may be taken regarding this matter.
3. Approval of the minutes of the January meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Approval of monthly bills.
7. Discussion of the Reservoir Upgrade Project with SEH Representatives. Action may be taken to approve capital expenditures pertaining to this project and authorize payment to vendors. Additional action may be taken regarding this matter.
8. Discussion regarding meeting with member utilities, the Commission, and the Wisconsin Department of Natural Resources (WDNR) regarding lead and copper monitoring. Discussion of a possible policy for lead and copper sample collection.
9. Discussion of proposed organizational changes, employee handbook changes, and related topics. Action may be taken to adopt a resolution regarding changes to staff organization, job descriptions, policies, and compensation.
10. Discussion of shoreline erosion at the Klode Park Raw Water Pumping Station.
11. Manager’s report.
12. Date and time of the next regular Commission Meeting.
13. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2020Meeting_02122020_revised.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2020Meeting_02122020_amended_03112020.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2020Meeting_02122020_postmeeting.pdf)
