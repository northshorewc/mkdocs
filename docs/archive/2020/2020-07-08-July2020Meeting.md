* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **July 8, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:16 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the June meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
7. Discussion of the High Service Pump #5 Removal Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
8. Discussion of results from crane inspection and possible approval to make recommended repairs.
9. Discussion of various factors that could impact the 2021 budget. This item may include discussion of staffing goals and objectives, employee pay and benefits, and potential strategies for funding capital projects.
10. Discussion of transmission mains and proposed strategies for locating and marking said mains.
11. Update of lead and copper rule compliance testing.
12. Presentation of monthly operations and maintenance report.
13. Date and time of the next regular Commission Meeting.
14. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_July2020Meeting_07082020_Amended.pdf)

* * *
## Minutes

*Not available at this time.*


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_August2020Meeting_07082020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_July2020Meeting_07082020_postmeeting.pdf)
