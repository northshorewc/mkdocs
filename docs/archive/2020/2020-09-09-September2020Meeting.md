* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 9, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:01 AM and ended at 9:30 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the August 12 meeting and the special meeting on August 19.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
7. Discussion of the purchase of a GPS Receiver for the location of the raw water transmission main. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to the vendor.
8. Discussion of the draft Unattended Operations Study from Baxter & Woodman.
9. Discussion and possible approval of the 2021 Operating Budget and Capital Improvement Budget.
10. Update of Lead and Copper Rule (LCR) compliance monitoring.
11. Presentation of monthly operations and maintenance report.
12. Date and time of the next regular Commission Meeting.
13. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_September2020Meeting_09092020.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_October2020Meeting_09092020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_September2020Meeting_09092020_postmeeting.pdf)
