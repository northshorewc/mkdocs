* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 11, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:50 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the October meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
7. Discussion of High Service Pump 5 Removal Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to the vendor.
8. Discussion and possible action to accept chemical quotations for 2021.
9. Discussion of preliminary report provided by Process Research Solutions regarding lead and copper monitoring and recommendations for moving forward.
10. Presentation of monthly operations and maintenance report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_November2020Meeting_11112020.pdf)


* * *
## Minutes

*Not available at this time.*


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_December2020Meeting_11112020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_November2020Meeting_11112020_postmeeting.pdf)
