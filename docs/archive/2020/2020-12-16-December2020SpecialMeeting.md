* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 16, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:29 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Discussion of reservoir interconnect pipe problem that occurred on Wednesday, December 7, 2020. Action may be taken regarding this matter.
3. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
4. Adjournment

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_Special_12162020.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2021Meeting_Special_12162020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2020SpecialMeeting_12162020_postmeeting.pdf)
