* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **June 10, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:35 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the May meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
7. Discussion of the High Service Pump #5 Removal Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
8. Update of lead and copper rule compliance monitoring scheduled to start July 1, 2020.
9. Discussion of the effort to conduct a joint Risk and Resilience Assessment (RRA) and create a joint Emergency Response Plan (ERP).
10. Presentation of monthly operations and maintenance report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_June2020Meeting_06102020.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_July2020Meeting_06102020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_June2020Meeting_06102020_postmeeting.pdf)
