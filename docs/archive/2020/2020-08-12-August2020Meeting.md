* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 12, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:08 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the July meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
7. Discussion of the High Service Pump #5 Removal Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
8. Discussion and possible action to authorize renewal of causality and property insurance policies.
9. Discussion of Families First Coronavirus Response Act (FFCRA) and possible action regarding this matter.
10. Discussion of anticipated capital allocation rates for 2021.
11. Discussion of the preliminary 2021 Operating Budget and Capital Improvement Budget.
12. Discussion of the ownership of transmission mains. Action may be taken regarding this matter.
13. Update of Lead and Copper Rule (LCR) compliance monitoring.
14. Presentation of monthly operations and maintenance report.
15. Date and time of the next regular Commission Meeting.
16. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2020Meeting_08122020.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2020Meeting_08122020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2020Meeting_08122020_postmeeting.pdf)
