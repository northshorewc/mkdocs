* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 9, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 9:17 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Presentation of the Unattended Operations Study by Baxter & Woodman. Discussion and possible action may be taken regarding this matter.
3. Approval of the minutes of the November meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Approval of monthly bills.
7. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
8. Discussion of High Service Pump 5 Removal Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to the vendor.
9. Discussion of current lead and copper corrosion control program and possible changes in 2021.
10. Presentation of monthly operations and maintenance report.
11. Date and time of the next regular Commission Meeting.
12. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2020Meeting_12092020.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2021Meeting_12092020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2020Meeting_12092020_postmeeting.pdf)
