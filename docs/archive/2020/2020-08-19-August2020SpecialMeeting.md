* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A special Commission Meeting was held on **August 19, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:05 AM and ended at 8:15 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Discussion and possible action to authorize renewal of causality and property insurance policies as proposed by R&R Insurance.
3. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_Special_08192020.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2020Meeting_special_08192020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2020Meeting_Special_08192020_postmeeting.pdf)
