* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 14, 2020** using an internet-based online conferencing platform called Zoom. The meeting started at 8:00 AM and ended at 8:30 AM.

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the September meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Discussion of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to vendors.
7. Discussion of High Service Pump 5 Removal Project. Action may be taken to approve capital expenditures pertaining to this project and to authorize payment to the vendor.
8. Update of Lead and Copper Rule (LCR) compliance monitoring.
9. Presentation of monthly operations and maintenance report.
10. Date and time of the next regular Commission Meeting.
11. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2020Meeting_10142020.pdf)


* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2020Meeting_10142020_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2020Meeting_10142020_postmeeting.pdf)
