* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **November 13, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the October meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to these projects and authorize payment to vendors.
7. Discussion and possible action to accept chemical quotations for 2020.
8. Discussion and possible action to adopt resolution for designating accounts and setting procedures for the management of funds.
9. Discussion of American Water Works Association (AWWA) certificate program for Utility Risk and Resilience. Possible action may be taken to authorize Plant Manager to enroll in the program.
10. Discussion of proposed staffing changes and discussion of enticement to get the attention of prospective employees. Possible action may be taken regarding this matter.
11. Discussion of unattended operations and how much it could cost the Commission. Possible action may be taken on this matter.
12. Discussion and possible action to approve 5-year capital improvement plan.
13. Discussion of a major electrical outage at Klode Park and actions taken by Plant Staff to resolve situation.
14. Manager’s report.
15. Date and time of the next regular Commission Meeting.
16. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_November2019Meeting_11132019.pdf)

* * *
## Minutes

*Not available at this time.*


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_December2019Meeting_11132019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_November2019Meeting_11132019_postmeeting.pdf)
