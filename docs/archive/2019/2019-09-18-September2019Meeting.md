* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **September 18, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Discussion of lead and copper monitoring following change of corrosion control chemical in July. Process Research Solutions (PRS) will present data and provide recommendations for moving forward. Possible action may taken to accept proposal provided by PRS.
3. Approval of the minutes of the August meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Approval of monthly bills.
7. Status report of the Reservoir Upgrade Project and the Air Dryer Replacement Project. Action may be taken to approve capital expenditures pertaining to these projects and authorize payment to vendors.
8. Discussion regarding meeting with City of Glendale, Wisconsin Department of Natural Resources (WDNR), and Commission regarding emergency response plans. Possible action may be taken by the Commission regarding this matter.
9. Discussion of proposed staffing changes in 2020.
10. Discussion and possible action to adopt the proposed 2020 Operating Budget and Capital Improvement Budget.
11. Discussion and possible action to approve 5 and 20 year capital improvement plans.
12. Manager’s report.
13. Date and time of the next regular Commission Meeting.
14. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_September2019Meeting_09182019.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_October2019Meeting_09182019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_September2019Meeting_09182019_postmeeting.pdf)
