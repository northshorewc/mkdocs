* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **December 11, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the November meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the Reservoir Upgrade Project with SEH representative via conference call. Discussion may include topics such as project administration, work schedule, quality of work products, communications, customer service, and plan for meeting compliance deadline.
7. Discussion of invoices received for work performed on the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to this project and authorize payment to vendors.
8. Update regarding proposed lead and copper rule changes, emergency response plan revisions, and 2020 lead and copper monitoring requirements for member communities.
9. Manager’s report.
10. Date and time of the next regular Commission Meeting.
11. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_December2019Meeting_12112019.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_January2020Meeting_12112019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_December2019Meeting_12112019_postmeeting.pdf)
