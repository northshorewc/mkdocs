* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **October 9, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Discussion by Baker Tilly regarding upcoming financial audit. Topics that may be discussed include proposed formatting changes to the financial statements and interpretation of certain aspects of the agreement as it pertains to fund balance.
3. Approval of the minutes of the September meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Approval of monthly bills.
7. Status report of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to these projects and authorize payment to vendors.
8. Discussion and possible action to accept proposal from Process Research Solutions for professional services pertaining to lead and copper corrosion control.
9. Discussion of proposed staffing changes.
10. Discussion and possible action to approve 5 and 20 year capital improvement plans.
11. Manager’s report.
12. Date and time of the next regular Commission Meeting.
13. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_October2019Meeting_10092019.pdf)

* * *
## Minutes


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_November2019Meeting_10092019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_October2019Meeting_10092019_postmeeting.pdf)
