* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **March 13, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the February meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Presentation by Arch Electric regarding the possibility of installing solar panels after the completion of the anticipated reservoir upgrade project in 2020. The Commission may take action regarding this matter.
7. Status report of the High Service Pump #4 VFD replacement project and recent pump inspection. Action may be taken to approve pump repairs.
8. Discussion and possible acceptance of the 2018 Financial Statements and other required communications prepared by Baker Tilly.
9. Discussion and possible approval of check to the City of Glendale and invoices to Villages of Fox Point and Whitefish Bay related to year-end accounting.
10. Discussion of the proposal submitted by SEH to support Commission in locating and assessing the condition of the raw water transmission main. The Commission may take action regarding this matter.
11. Discussion of the anticipated reservoir upgrade project in 2020 its effect on members and wholesale customer.
12. Discussion of WDNR’s letter to the Commission and member utilities regarding the upcoming project to change its corrosion control chemical. Possible action may be taken regarding this matter.
13. Discussion of the 2018 Annual Water Quality Report. Action may be taken to accept the report.
14. Manager’s report.
15. Date and time of the next regular Commission Meeting.
16. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_March2019Meeting_03132019.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_April2019Meeting_03132019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_March2019Meeting_03132019_postmeeting.pdf)
