* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **August 14, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the July meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the Reservoir Upgrade Project. Action may be taken to approve capital expenditures pertaining to these projects and authorize payment to vendors.
7. Discussion and possible action to decide if reservoirs 1 and 2 should be covered with membrane and soil as part of the upcoming reservoir upgrade project.
8. Discussion of historical and projected wholesale water rates for the City of Mequon. Possible action may be taken regarding this item.
9. Discussion and possible action to authorize renewal of causality and property insurance policies.
10. Discussion of anticipated capital allocation rates for 2020.
11. Discussion of the preliminary 2020 Operating Budget and Capital Improvement Budget.
12. Discussion and possible action to approve resolution to continue participating in the Wisconsin Public Employers group health insurance program (WPE-GHIP).
13. Manager’s report.
14. Date and time of the next regular Commission Meeting.
15. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_August2019Meeting_08142019.pdf)

* * *
## Minutes

*Not available at this time.*


[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_September2019Meeting_08142019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_August2019Meeting_08142019_postmeeting.pdf)
