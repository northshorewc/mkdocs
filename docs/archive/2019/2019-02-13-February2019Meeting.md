* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **February 13, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Presentation of the 2018 draft financial statements and other required communications by Baker Tilly. Possible action may be taken regarding this matter.
3. Approval of the minutes of the January meeting.
4. Monthly report of plant operations.
5. Discussion of annual budget.
6. Approval of monthly bills.
7. Status report of the High Service Pump #4 VFD replacement effort. Action may be taken to approve capital expenditures pertaining to this project.
8. Discussion of proposals for capital project 2019-1, Engineering Services for Reservoir Upgrade Project. John Edlebeck and Eric Kiefer will give a report to the Commission regarding interviews and reference checks. Possible action may be taken to select a proposal.
9. Update regarding the Safe Drinking Water Loan Program and its applicability to the upcoming reservoir upgrade project.
10. Presentation by Paul Pasko of SEH to discuss locating and condition assessment alternatives of the raw water transmission main. Possible action may be taken regarding this matter.
11. Manager’s report.
12. Pursuant to section 19.85 (1) (c) of Wisconsin Statutes, the Commission reserves the right to convene in closed session to discuss and consider wages rates and salaries of specified employees. The Commission may reconvene into open session to take action regarding this matter.
13. Date and time of the next regular Commission Meeting.
14. Adjournment.


[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_February2019Meeting_02132019.pdf)
* * *
## Minutes

<!-- *Not available at this time.* -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_March2019Meeting_02132019_signed_revised03132019.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_February2019Meeting_02132019_postmeeting.pdf)
