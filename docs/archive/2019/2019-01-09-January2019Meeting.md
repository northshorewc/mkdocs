* * *
![nswc-logo](../../img/nswc-logo-long.png)
* * *

A Commission Meeting was held on **January 9, 2019** at the Water Filtration Plant located at 400 West Bender Road, Glendale, WI 53217. ([Map](https://goo.gl/maps/RG9Wj))

* * *
## Agenda

1. Call to order by the Chair.
2. Approval of the minutes of the December meeting.
3. Monthly report of plant operations.
4. Discussion of annual budget.
5. Approval of monthly bills.
6. Status report of the SCADA Upgrade Project and the Filter Improvements Project. Action may be taken to approve capital expenditures pertaining to these projects and authorize payment to vendors.
7. Discussion of 2018 capital additions and retirements. Possible action may be taken regarding this matter.
8. Discussion of the process being taken to solicit proposals for capital project 2019-1, Engineering Services for Reservoir Upgrade Project. Possible action may be taken regarding this matter.
9. Discussion of proposal from Pure Technologies Inc., to perform condition assessment and other services pertaining to the raw water transmission main.
10. Presentation by Process Research Solutions regarding general strategies that can be used to improve water quality at the plant and in the distribution systems. Discussion to follow presentation.
11. Discussion of annual report.
12. Manager’s report.
13. Date and time of the next regular Commission Meeting.
14. Adjournment.

[:material-file-download: Download Agenda](../../agenda/NSWC_Agenda_January2019Meeting_01092019.pdf)

* * *
## Minutes

<!-- *Not available at this time.* -->
<!-- ## Minutes (*UNOFFICIAL*) -->

[:material-file-download: Download Minutes](../../minutes/NSWC_minutes_February2019Meeting_revised_01092019_signed.pdf)

* * *

## Packet

[:material-file-download: Download Packet](../../materials/NSWC_packet_January2019Meeting_01092019_postmeeting.pdf)
