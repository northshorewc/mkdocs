* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

## Treatment Process

Our treatment process starts when water enters our intake crib located in Lake Michigan. Bar screens prevent large objects from entering the intake, and a mussel control system prevents zebra and quagga mussels from accumulating in our pipes and equipment.

Gravity forces the water to fill a large shorewell that has several raw water pumps submersed in it. After passing through another screen, lake water is pumped from this station to the main treatment plant.

Upon arriving at the main facility, aluminum sulfate (alum) and polymer are added to promote settling of solids. This water then travels very slowly through a system of rectangular basins that provide a location for particulate matter to accumulate.

Rapid sand filters remove the remaining particulate matter and clean water is stored in a clearwell below the filters. There are intermediate pumps (sometimes referred to as transfer or low lift pumps) which force the filtered water through a fully redundant ultraviolet (UV) disinfection system which inactivates pathogens such as cryptosporidium. Before entering the UV reactors, fluoride is added to the water to prevent tooth decay. After passing through the UV reactors, sodium hypochlorite (a form of chlorine) is added to the water to disinfect any remaining pathogens.

Underground piping directs the water to our chlorine contact-time clearwells. While in these clearwells, chlorine has sufficient time to disinfect the water.

Our high service pumps draws water from these clearwells into a chemical feed vault where ammonium hydroxide (also referred to as aqua ammonia) and phosphate are added. The ammonium hydroxide converts the chlorine to another disinfectant, chloramine, that is used by neighboring communities and can reduce disinfection by-products formation in the distribution system. Phosphate is also added at this point to reduce lead and copper leaching within pipes and plumbing fixtures in your home.

Water leaving the main facility is metered as it enters the mains of the member communities. Within the distribution systems of Fox Point, Glendale, and Whitefish Bay, there are metered interconnections that are kept open to improve pressure and water quality. The NSWC also reads and maintains these meters.

* * *

## More Information about Water

For additional information about water quality on the internet, please visit WDNR’s website at [http://dnr.wi.gov/topic/DrinkingWater](http://dnr.wi.gov/topic/DrinkingWater) or the EPA’s website at [http://www.epa.gov/safewater](http://www.epa.gov/safewater).
