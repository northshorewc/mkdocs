* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

[2024 Annual Water Quality Report - html](https://northshorewc.com/CCR2024/) <br >
[2024 Annual Water Quality Report - pdf](https://www.northshorewc.com/CCR2024.pdf)
* * *

[2023 Annual Water Quality Report - html](https://northshorewc.com/CCR2023/) <br >
[2023 Annual Water Quality Report - pdf](https://www.northshorewc.com/CCR2023.pdf)
* * *

[2022 Annual Water Quality Report - html](https://northshorewc.com/CCR2022/) <br >
[2022 Annual Water Quality Report - pdf](https://www.northshorewc.com/CCR2022.pdf)
* * *

[2021 Annual Water Quality Report - html](https://northshorewc.com/CCR2021/) <br >
[2021 Annual Water Quality Report - pdf](https://www.northshorewc.com/CCR2021.pdf)
* * *

[2020 Annual Water Quality Report - html](https://northshorewc.com/CCR2020/) <br >
[2020 Annual Water Quality Report - pdf](https://www.northshorewc.com/CCR2020.pdf)
* * *

[2019 Annual Water Quality Report - html](https://northshorewc.github.io/ccr2019/) <br >
[2019 Annual Water Quality Report - pdf](http://www.northshorewc.com/CCR2019.pdf)
* * *

[2018 Annual Water Quality Report - html](https://northshorewc.github.io/ccr2018/) <br >
[2018 Annual Water Quality Report - pdf](http://www.northshorewc.com/CCR2018.pdf)
* * *

[2017 Annual Water Quality Report - html](https://northshorewc.github.io/ccr2017/) <br >
[2017 Annual Water Quality Report - pdf](http://www.northshorewc.com/CCR2017.pdf)
* * *

[2016 Annual Water Quality Report - html](https://northshorewc.github.io/ccr2016/) <br >
[2016 Annual Water Quality Report - pdf](http://www.northshorewc.com/CCR2016.pdf)
* * *

[2015 Annual Water Quality Report](https://goo.gl/Lh78Z8)
* * *

[2014 Annual Water Quality Report](https://goo.gl/CzJYFg)
* * *

[2013 Annual Water Quality Report](https://goo.gl/bo2a1H)
* * *

[2012 Annual Water Quality Report](https://goo.gl/cjiko5)
* * *

[2011 Annual Water Quality Report](https://goo.gl/RJCsK4)
* * *

[2010 Annual Water Quality Report](https://goo.gl/AXXihd)
* * *

[2009 Annual Water Quality Report](https://goo.gl/42vz9v)
* * *

[2008 Annual Water Quality Report](https://goo.gl/FkcKTb)
* * *

[2007 Annual Water Quality Report](https://goo.gl/SvPify)
* * *

[2006 Annual Water Quality Report](https://goo.gl/FNS8KV)
* * *

[2005 Annual Water Quality Report](https://goo.gl/WRmCeR)
* * *

[2004 Annual Water Quality Report](https://goo.gl/4EEZJs)
* * *
