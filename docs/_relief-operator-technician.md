* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

**Looking to Fill this Position Right Now!**
**[Click here if you are interested.](https://forms.gle/gU6e66hd6hfNeNjd9)**

* * *

## General Description
* * *

This is a skilled position responsible for assisting in the operation of the water treatment plant and related equipment. Work involves maintaining correct water levels, draining, washing and refilling water filters, and adjusting chemical feed. Work also includes maintaining, calibrating, and performing minor repair of instrumentation. This position will work a varied schedule to relieve Operator Technicians that are absent and will normally collect water samples in the distribution system.

## North Shore Water Commission Description
* * *

The North Shore Water Commission (NSWC) is a progressive drinking water treatment facility located in the City of Glendale, Wisconsin. It supplies water to its member utilities in Fox Point, Glendale, and Whitefish Bay and also supplies water on a wholesale basis to the City of Mequon.

## Location
* * *

This position will primarily work at our main office at [400 W Bender Rd, Glendale, WI](https://goo.gl/maps/RG9Wj).

## Supervisor
* * *

This position reports to the Operations Supervisor.

## Work Schedule
* * *

Schedule varies depending on staffing needs. This position works a varied which is influenced by absences of plant staff. When there are no vacancies or shifts to fill--and all Operator Technician positions are working regular shifts--the position can work up to 40 hours using a flexible schedule.

<!-- **Schedule A (fully staffed)**

| No.     | Day         | Shift                    | Hours |
|---------|-------------|--------------------------|-------|
| Day 1:  | Monday      | 11:00 PM to 7:00 AM      | (8h)  |
| Day 2:  | Tuesday     | 7:00 PM to 7:00 AM       | (12h) |
| Day 3:  | Wednesday   | 7:00 PM to 7:00 AM       | (12h) |
| Day 4:  | Thursday    | 11:00 PM to 7:00 AM      | (8h)  |
| Day 5:  | Friday      | 11:00 PM to 7:00 AM      | (8h)  |
| Day 6:  | Saturday    | 7:00 PM to 7:00 AM       | (12h) |
| Day 7:  | Sunday      | 7:00 PM to 7:00 AM       | (12h) |
| Day 8:  | Monday      | OFF                      |       |
| Day 9:  | Tuesday     | OFF                      |       |
| Day 10: | Wednesday   | OFF                      |       |
| Day 11: | Thursday    | 3:00 PM to 11:00 PM      | (8h)  |
| Day 12: | Friday      | 3:00 PM to 11:00 PM      | (8h)  |
| Day 13: | Saturday    | 7:00 AM to 7:00 PM       | (12h) |
| Day 14: | Sunday      | 7:00 AM to 7:00 PM       | (12h) |
| Day 15: | Monday      | 3:00 PM to 11:00 PM      | (8h)  |
| Day 16: | Tuesday     | OFF                      |       |
| Day 17: | Wednesday   | OFF                      |       |
| Day 18: | Thursday    | OFF                      |       |
| Day 19: | Friday      | 3:00 PM--flexible/relief | (TBD) |
| Day 20: | Saturday    | flexible/relief          | (TBD) |
| Day 21: | Sunday      | flexible/relief          | (TBD) |
| Day 22: | Monday      | flexible/relief          | (TBD) |
| Day 23: | Tuesday     | flexible/relief          | (TBD) |
| Day 24: | Wednesday   | flexible/relief          | (TBD) |
| Day 25: | Thursday    | flexible/relief          | (TBD) |
| Day 26: | Friday      | flexible/relief--3:00 PM | (TBD) |
| Day 27: | Saturday    | OFF                      |       |
| Day 28: | Sunday      | OFF                      |       |


**Schedule B (not fully staffed)**

| No.     | Day         | Shift                    | Hours |
|---------|-------------|--------------------------|-------|
| Day 1:  | Monday      | 11:00 PM to 7:00 AM      | (8h)  |
| Day 2:  | Tuesday     | 7:00 PM to 7:00 AM       | (12h) |
| Day 3:  | Wednesday   | 7:00 PM to 7:00 AM       | (12h) |
| Day 4:  | Thursday    | 11:00 PM to 7:00 AM      | (8h)  |
| Day 5:  | Friday      | 11:00 PM to 7:00 AM      | (8h)  |
| Day 6:  | Saturday    | 7:00 PM to 7:00 AM       | (12h) |
| Day 7:  | Sunday      | 7:00 PM to 7:00 AM       | (12h) |
| Day 8:  | Monday      | OFF                      |       |
| Day 9:  | Tuesday     | OFF                      |       |
| Day 10: | Wednesday   | OFF                      |       |
| Day 11: | Thursday    | 3:00 PM to 11:00 PM      | (8h)  |
| Day 12: | Friday      | 3:00 PM to 11:00 PM      | (8h)  |
| Day 13: | Saturday    | 7:00 AM to 7:00 PM       | (12h) |
| Day 14: | Sunday      | 7:00 AM to 7:00 PM       | (12h) |
| Day 15: | Monday      | 3:00 PM to 11:00 PM      | (8h)  |
| Day 16: | Tuesday     | OFF                      |       |
| Day 17: | Wednesday   | OFF                      |       |
| Day 18: | Thursday    | OFF                      |       |
| Day 19: | Friday      | OFF                      |       |
| Day 20: | Saturday    | OFF                      |       |
| Day 21: | Sunday      | OFF                      |       |
 -->


## Pay
* * *

**Hourly rate of $25.21 to $32.50 depending on qualifications and years of experience**

## Benefits
* * *

* Health insurance
* Life insurance
* Paid holidays (6 days; however, Relief Operator Technician works holidays if scheduled)
* Paid sick leave, vacation, and personal leave
* Section 125 flexible benefit plan
* Pension and retirement savings plan

## Other Information
* * *

* Relief Operator Technicians are allowed to engage in a number of independent projects depending on skill level and interest.
* Background check will be conducted before hiring.

## Essential Job Functions
* * *

*OPERATION DUTIES - 80% OF TIME*

Maintains correct water levels at all stages of the water treatment process.

Maintains correct pressure at all stages of water treatment process.

Starts and stops process equipment.

Makes operational and security rounds at frequency and time determined by management.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li> Walking throughout entire building
<li> Pushing buttons at designated stations
<li> Making visual observations of equipment and facilities
<li> Listening to equipment and other background noise
<li> Using other senses and observation techniques as needed
<li> Perform routine water quality analyses required during each round
</p>
</details>

Operates and maintains filters.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Draining
<li>Washing
<li>Refilling
<li>Operating via supervisory control and data acquisition (SCADA) system
<li>Operating via local controls
<li>Modifying operating parameters
<li>Performing filter drop test
</p>
</details>

Operates, adjusts, and calibrates chemical feed systems.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Sodium hypochlorite
<li>Aqua ammonia
<li>Coagulant
<li>Veligon TL-M
<li>Fluoride
<li>Polymer
<li>Phosphate
</p>
</details>

Operates all systems in plant using manual/local controls, as well as automatic/remote controls.

Performs public relations duties.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Answering telephone in polite and courteous manner
<li>Greeting visitors at door
<li>Collecting necessary information and materials from clients requesting water testing.
</p>
</details>

Performs laboratory tests.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Chlorine residual (free and total)
<li>Free ammonia residual
<li>Nitrite analysis
<li>Nitrate analysis
<li>Fluoride residual
<li>Phosphate residual
<li>Hardness
<li>Alkalinity
<li>pH
<li>Heterotrophic plate count
<li>Most-Probable-Number (MPN) of Total Coliform and E. Coli.
<li>Presence Absence of Total Coliform and E. Coli.
<li>Odor
<li>Jar test for coagulant
<li>Conductivity
</p>
</details>

Operates laboratory equipment.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Spectrophotometer
<li>pH meter
<li>Turbidimeter
<li>Autoclave
<li>Dry sterilizer
</p>
</details>

Mixes and dilutes laboratory chemicals and reagents as needed.

Maintains accurate records and reports.

Operates sludge removal system.

Troubleshoots and responds to operational problems.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Chemical feed malfunction
<li>Erratic pumpage
<li>Erratic performance of filters
<li>Power failure
<li>Equipment failure
</p>
</details>

Use computers for record-keeping, reporting, and supervisory control of treatment processes.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Using mouse and keyboard to enter data
<li>Changing setpoints and operational parameters
<li>Data entry into spreadsheet, forms or database
<li>Operating word processing, spreadsheet, and database programs
<li>Printing lab results
</p>
</details>

Completes log sheets.

Understands chemical, mechanical, and hydraulic principles of water treatment processes.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Flocculation
<li>Sedimentation
<li>Sludge removal
<li>Filtration
<li>Disinfection
<li>Filter backwash
<li>Water transfer via pumps
<li>Clearwell storage
<li>Reservoir storage
<li>Washwater storage
<li>Pressurization of distribution system
</p>
</details>

Understands electrical system layout for Water Treatment Plant and Klode Park Pumping Station.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Transfer switchgear location
<li>Electrical buss configuration
<li>Location of high voltage areas
<li>Location of electrical breakers and fuses
</p>
</details>

Performs tasks on electrical.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Resetting breakers
<li>Starting and stopping emergency power generators
</p>
</details>

*MAINTENANCE DUTIES - 15% OF TIME*

Checks, cleans, and performs light maintenance on process and monitoring equipment.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Flow meters
<li>Pressure gauges
<li>Level indicating devices
<li>Chemical mixing chambers
<li>Basins
<li>Lift pumps
<li>Chemical feed pumps
<li>Chemical analyzers
<li>Laboratory equipment
<li>Air compressors
<li>Emergency power generators
<li>Boilers
<li>Chemical feed injectors
<li>Static mixers
<li>Valves
<li>Computers
<li>Electrical panels
<li>Uninterruptible power supplies
</p>
</details>

Installs and replaces components of instruments as part of light maintenance.

<details><summary>This includes, but is not limited to...</summary>
<p>
<li>Replacing tubing and lamps for analyzers
<li>Replacing consumable products
</p>
</details>

Performs routine maintenance, cleaning and custodial tasks.

Cleans and paints equipment and facilities as needed.

Maintains a clean working environment.

*OTHER DUTIES - 5% OF TIME*

Takes inventory of chemicals and other supplies.

Assists with emergency breakdowns.

Assists Mechanical Technician as needed.

Assists Operations Supervisor as needed.

Assists Maintenance Supervisor as needed.

Assists Plant Manager as needed.

*IMPORTANT NOTE*

Regular and predictable attendance, as well as compliance with the Commission’s policies and procedures, is an essential function of the job.

Nothing in this job description limits the Commission’s right to assign or reassign duties and responsibilities to this job at any time. The job description may be changed at any time by the Commission. The duties listed above are intended only as illustrations of the various types of work that may be performed. The omission of specific statements of duties does not exclude them from the position if the work is similar, related or a logical assignment to the position. All job functions must be performed in a manner satisfactory to the Commission.

## Requirements of Work
* * *

This position must possess the following credentials:

* Possess and maintain WDNR waterworks operator certification--subclass surface water (S)--within 3 months of hiring.
* Possess a valid driver's license with a good driving record.

This position must have experience and training which provides, with or without reasonable accommodation when applicable, for the following abilities, skills, and knowledge:

ESSENTIAL ABILITIES

* Ability to work safely in and around various chemicals.
* Ability to maintain effective relations with other employees and to deal with the public in a courteous and tactful manner.
* Ability to communicate effectively with other people.
* Ability to understand and follow oral and written instructions.
* Ability to read and write documents in English.

SKILLS THAT MUST BE ACQUIRED WITHIN 3 MONTHS OF BEING IN THE POSITION

* Use of computers and associated software for use in email, data entry, word processing, and spreadsheet.
* Use of supervisory control and data acquisition (SCADA) to supervise and control treatment plant equipment and processes.
* Use of common hand and power tools.
* Use of laboratory equipment and analyzers.

ESSENTIAL KNOWLEDGE THAT MUST BE ACQUIRED OR LEARNED ON THE JOB WITHIN 6 MONTHS OF BEING IN THE POSITION

* Basic electrical, mechanical, pneumatic, and hydraulic principles.
* Water treatment plant operations.
* Chemicals used in a water treatment plant and basic chemistry.
* Basic math and algebra to perform calculations.
* Laboratory techniques.
* Awareness of workplace hazards and safety precautions.

## Environmental/Working Conditions of Position
* * *

The following environmental characteristics described here are representative of those an employee encounters while performing the essential functions of this job. Reasonable accommodations may be made to enable individuals with disabilities to perform essential functions.

* Standing, walking and sitting.
* Climbing, using legs and feet, balancing, bending and twisting.
* Reaching, feeling, talking and hearing.
* Far vision at 20 feet or further and near vision at 20 inches or less, with glasses if needed.
* Stooping, kneeling and crouching.
* Lift and/or move up to 50 lbs.
* Fingering such as to perform chemical analysis in the laboratory, change fittings, cut and place tubing, and other similar actions.
* Standing for at least 30 continuous minutes in the laboratory while conducting analyses.
* Ability to work in wet, damp, and humid conditions.
* Ability to work in cold and damp areas for extended periods of time.

## Interested?
* * *

Please visit and complete the Interest Form at [https://forms.gle/gU6e66hd6hfNeNjd9](https://forms.gle/gU6e66hd6hfNeNjd9). If you cannot access the form, please contact Eric Kiefer at info@northshorewc.com or (414) 963-0160. This position may be advertised until filled.


* * *
**Dated 12/28/2022**
