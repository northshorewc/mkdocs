* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

<div class="your-class">
  <div><img data-lazy="img/Klode_duo_1.jpg"></div>
  <div><img data-lazy="img/Bender_duo_1.jpg"></div>
  <div><img data-lazy="img/Fountain_duo_1.jpg"></div>
</div>

<!-- ![nswc-building](img/Building_Bender_duo_1.JPG) -->

The North Shore Water Commission (NSWC) operates and maintains the Water Filtration Plant at 400 West Bender Road in Glendale as well as the Raw Water Pumping Station located in Whitefish Bay. The NSWC is responsible for purifying and pumping potable water to its member utilities which include Fox Point, Glendale, and Whitefish Bay. In addition, water is also provided to some areas served by Mequon Water Utility via a wholesale water agreement.

<!-- * * *

## Lead and Copper Monitoring

In partnership with our member water utilities, we have selected households in Fox Point, Glendale, and Whitefish Bay to participate in our lead and copper monitoring program. Monitoring sites were selected based on stringent criteria determined by the United States Environgmental Protection Agency (USEPA) and the Wisconsin Department of Natural Resources (WDNR). Those households will receive a letter from the Commission describing the monitoring program as well as other information about lead and copper.

[Click here for more information.](lcr.md) -->

<!-- * * * -->
<!--
## Intern Opening

If you are interested in a career in the water industry, you may want to consider becoming an Intern. [Click here for more information](internship.md)

-->
<!-- * * *

## Relief Operator Technician Opening

This is a skilled position responsible for assisting in the operation of the water treatment plant and related equipment. Work involves maintaining correct water levels, draining, washing and refilling water filters, and adjusting chemical feed. Work also includes maintaining, calibrating, and performing minor repair of instrumentation. This position will work a varied schedule to relieve Relief Operator Technicians that are absent and will normally collect water samples in the distribution system. [Click here for more information](relief-operator-technician.md) -->

* * *

## Water Utilities

Each water utility is responsible for connecting customers to the water system, metering water use, billing, and responding to distribution problems such as main breaks. You can find out who your water utility is by checking your water bill. If you experience a water-related problem or have questions about your water bill, please contact your water utility for assistance.

|                        Fox Point Water Utility                         |                  Glendale Water Utility                  |              Whitefish Bay Water Utility               |
|:----------------------------------------------------------------------:|:--------------------------------------------------------:|:------------------------------------------------------:|
|             ![Fox Point Logo](img/FPTLogoSmall-150.png)              |       ![Glendale Logo](img/glendale-logo-150.png)        |    ![Whitefish Bay Logo](img/wfb-logo-150-250.png)     |
|                             (414) 351-8900                             |                      (414) 228-1719                      |                     (414) 962-6690                     |
| [https://www.villageoffoxpoint.com](https://www.villageoffoxpoint.com/) | [https://www.glendale-wi.org](https://www.glendale-wi.org/) | [https://www.wfbvillage.gov](https://www.wfbvillage.gov/) |


* * *

## Associations

The NSWC is a proud member of the following associations:

| Association                                                              | Logo                                                               |
|:-------------------------------------------------------------------------|:-------------------------------------------------------------------|
| [American Water Works Association](https://www.awwa.org/)                | ![AWWA Logo](img/awwa-logo-150-729.png)                            |
| [Wisconsin Section AWWA](http://www.wiawwa.org/)                         | ![Wisconsin Water Works Association Logo](img/wiawwa-logo-150.png) |
| [Water Research Foundation](http://www.waterrf.org/)                     | ![Water Research Foundation Logo](img/wrf_logo.png)        |
| [West Shore Water Producers Association](http://www.westshorewater.org/) | ![West Shore Water Producers Association](img/wswpa-logo.png)         |


<!-- | [The Water Council](http://thewatercouncil.com/)                         | ![Water Council Logo](img/water-council-logo-150-729.png)          | -->
