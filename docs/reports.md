* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

## Annual Water Quality Reports

[Click here to access the most recent report.](https://northshorewc.com/CCR2024/)

[Click here to access older reports.](ccrs.md)

* * *

## Annual Report

[2024 Annual Report](reports/NSWC_report_AnnualReport_2024_01102025.pdf)

[2023 Annual Report](reports/NSWC_report_AnnualReport_2023_02152024.pdf)

[2022 Annual Report](reports/NSWC_report_AnnualReport_2022_02232023.pdf)

[2021 Annual Report](reports/NSWC_report_AnnualReport_2021_02222022.pdf)

[2020 Annual Report](reports/NSWC_report_AnnualReport_2020_01202021.pdf)

[2019 Annual Report](reports/NSWC_report_AnnualReport_2019_01202021.pdf)

[2018 Annual Report](reports/NSWC_report_AnnualReport2018__01072019.pdf)

[2017 Annual Report](https://goo.gl/EzjyWk)

[2016 Annual Report](https://goo.gl/846BcQ)

* * *

## Budgets

[2025 Capital Budget](budgets/CAPBudget2025-APPROVED-09112024.pdf)

[2025 Operating Budget](budgets/OPBudget2025-APPROVED-09112024.pdf)

[2024 Capital Budget](budgets/CAPBudget2024-APPROVED-09132023.pdf)

[2024 Operating Budget](budgets/OPBudget2024-APPROVED-09132023.pdf)

[2023 Capital Budget](budgets/CAPBudget2023-APPROVED-09142022.pdf)

[2023 Operating Budget](budgets/OPBudget2023-APPROVED-09142022.pdf)

[2022 Capital Budget](budgets/CAPBudget2022-APPROVED-09082021.pdf)

[2022 Operating Budget](budgets/OPBudget2022-APPROVED-09082021.pdf)

[2021 Capital Budget](budgets/CAPBudget2021-APPROVED-09092020.pdf)

[2021 Operating Budget](budgets/OPBudget2021-APPROVED-09092020.pdf)

[2020 Capital Budget](budgets/CAPBudget2020-APPROVED-09182019.pdf)

[2020 Operating Budget](budgets/OPBudget2020-APPROVED-09182019.pdf)

[2019 Capital Budget](budgets/CAPBudget2019-APPROVED-09122018.pdf)

[2019 Operating Budget](budgets/OPBudget2019-APPROVED-09122018.pdf)

[2018 Capital Budget](https://goo.gl/82hvZd)

[2018 Operating Budget](https://goo.gl/7bLyCC)

<!-- [2017 Capital Budget](https://goo.gl/tVgYux)

[2017 Operating Budget](https://goo.gl/T3jdXZ) -->

* * *

## Capital Improvement Plans

[2025 through 2030 Capital Improvement Plan - Approved 3/13/2024](cips/NSWC_CapitalImprovementPlan_March2024Meeting_Approved_03132024.pdf)

<!-- [2024 through 2029 Capital Improvement Plan - Approved 12/14/2022](cips/NSWC_CapitalImprovementPlan_December2022Meeting_Approved_12142022.pdf) -->

<!-- [2023 to 2028 Capital Improvement Plan - Approved 11/10/2021](cips/NSWC_CapitalImprovementPlan_November2021Meeting_Approved_11102021.pdf) -->

<!-- [5-Year Capital Improvement Plan](cips/NSWC_capitalImprovementPlan_2020to2024_09122018_Approved.pdf)

[20-Year Capital Improvement Plan](cips/NSWC_capitalImprovementPlan_2020to2039_09122018_Approved.pdf) -->

<!-- [5-Year Capital Improvement Plan](/cips/NSWC_capitalImprovementPlan_2019to2023_11082017_Approved.pdf)

[20-Year Capital Improvement Plan](/cips/NSWC_capitalImprovementPlan_2019to2038_11082017_Approved.pdf) -->

* * *

## Financial Statements

[2023 Financial Statements](financials/NSWC_report_accounting_FinancialStatements_2023_03132024.pdf)

[2022 Financial Statements](financials/NSWC_report_accounting_FinancialStatements_2022_03132023.pdf)

[2021 Financial Statements](financials/NSWC_report_accounting_FinancialStatements_2021_03092022.pdf)

[2020 Financial Statements](financials/NSWC_report_accounting_FinancialStatements_2020_02152021.pdf)

[2019 Financial Statements](financials/NSWC_report_accounting_FinancialStatements_2019_02262020.pdf)

[2018 Financial Statements](financials/NSWC_report_accounting_FinancialStatements_2018_02212019.pdf)

[2017 Financial Statements](financials/BakerTilly_report_FinancialStatements2017_March2018.pdf)

* * *

## Original Founding Agreement

The original founding agreement and its amendments and supplementary documents are linked below.

> The agreements that are linked below are provided for information and convenience in computerized form. All readers or users are advised that the full and official text of such documents is on file at the North Shore Water Commission main office, and available by request of the Plant Manager of the North Shore Water Commission.

[Founding Agreement with Amendments](https://goo.gl/yBGp5w)

<!--  -->

[Supplementary Documentation Referenced in Amended Founding Agreement](https://goo.gl/jEMdya)

* * *

## Revised and Restated Agreement for Water Supply System

A new agreement has be approved by the Village of Fox Point, City of Glendale, and Village of Whitefish Bay to replace our founding agreement and its amendments. This agreement allows the NSWC to operate and maintain the water supply system for its members and wholesale customer.

> The agreement that is linked below is provided for information and convenience in computerized form. All readers or users are advised that the full and official text of such documents is on file at the North Shore Water Commission Offices, and available by request of the Plant Manager of the North Shore Water Commission.

[Revised and Restated Agreement for Water Supply System](https://goo.gl/8gAKeL)

* * *

## Source Water Assessment

The North Shore Water Commission purifies water from Lake Michigan. The latest evaluation by the Wisconsin Department of Natural Resources (WDNR) indicates our source water quality is susceptible to pollution and contaminants. Preserving the water quality of Lake Michigan is essential to maintaining your drinking water quality.

[Source Water Assessment (2002)](https://goo.gl/Z9sGGm)
