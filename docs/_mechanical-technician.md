* * *
![nswc-logo](img/nswc-logo-long.png)
* * *

**Are you interested in this amazing job opportunity?**
**[Click here](https://forms.gle/BszMZzvtGaz6k1tp6)**

* * *

PLEASE NOTE: The earliest possible start date is December 1, 2020.

* * *

## General Description
* * *

This is a skilled position responsible for repair and maintenance of equipment in the Water Treatment Plant. Work involves troubleshooting problems, repair and maintenance of various types of equipment including control pumps, chemical feeders, detectors, analyzers,  filters, and computerized equipment. Work also includes operating the water treatment plant, lawn maintenance, and snow removal.

[Printer friendly version can be found here](https://docs.google.com/document/d/1l86M5ZG4wBdHeBfv2o_Zfo-vpW1A6zIoWsspgNAleM4/edit?usp=sharing)

## North Shore Water Commission Description
* * *

The North Shore Water Commission (NSWC) is a progressive drinking water treatment facility located in the City of Glendale, Wisconsin. It supplies water to its member utilities in Fox Point, Glendale, and Whitefish Bay and also supplies water on a wholesale basis to the City of Mequon.

## Location
* * *

Position will primarily work at our main office at [400 W Bender Rd, Glendale, WI](https://goo.gl/maps/RG9Wj).

## Work Schedule
* * *

* This position will normally work 7:00 AM to 3:00 PM, Monday through Friday, with some flexible scheduling.
Work schedule must consist of a minimum of 32 hours per week. Schedule must be worked during the following hours:
    * 7:00 AM to 3:00 PM: Monday, Thursday, or Friday
    * 7:00 AM to 7:00 PM: Tuesday or Wednesday


## Pay
* * *

* Hourly rate of $20.00 to $23.75 per hour depending on qualifications and years of experience

* After 6 months of employment, employees hired after 11/7/2019 are entitled to receive a phone of their choice up to $1,000.

## Benefits
* * *

* Health insurance - [State of Wisconsin Group Health Insurance Program - Local Traditional Health Plan (PO2)](https://etf.wi.gov/its-your-choice/2021/21et-2128)
</br>
Lowest cost plan in Milwaukee County for 2021 is Network Health
    * Employees pay monthly premium for single plan ($111.36) | family plan ($273.97)
    * No deductibles
* [Life insurance](https://etf.wi.gov/publications/et8903)
* Paid holidays (6 days)
* Paid sick leave, vacation, and personal leave
* [Section 125 flexible benefit plan](https://www.dbsbenefits.com/participant-resources/flexible-spending-accounts-fsa/)
* [Pension](https://etf.wi.gov/publications/et7100) and [retirement savings plan](https://docs.empower-retirement.com/EE/WisconsinWR/DOCS/Plan-Highlights.pdf)
    * 8th largest pension in USA, 25th largest pension in the world


## Other Information
* * *

* Requires high school diploma
* Requires considerable knowledge of electrical, pneumatic, and computerized equipment
* Requires ability to lift a minimum of 50 lbs
* Requires WDNR surface water certification within 6 months of hiring
* Possess a valid Driver's License in the State of Wisconsin and maintain a good driving record.

## Essential Job Functions
* * *

**Maintenance Duties - 80% of Time**

* Cleans, repairs, calibrates electrical and pneumatically operated recording instruments
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Venturi meters
<li> Magnetic flow meters
<li> Ultrasonic level indicators
<li> Electronic scales
<li> Pressure gauges
<li> Float switches
</p>
</details>

* Troubleshoots malfunctions and alarms at the Water Treatment Plant and Klode Park Pumping Station. In addition, troubleshoots control and monitoring systems at Glendale Standpipe, Whitefish Bay Tower, and the Fox Point Standpipe.
<details><summary>This includes, but is not limited to, the following types of malfunctions:</summary>
<p>
<li> Mechanical
<li> Electrical
<li> Plumbing
<li> Pneumatic
<li> Control
</p>
</details>

* Installs, maintains, and repairs AC and DC equipment, wiring, and light fixtures.

* Checks and maintains telemetry equipment.

* Programs and enters data into electronic devices, such as computers and programmable controllers.
<details><summary>Typical programs include, but are not limited to:</summary>
<p>
<li> Controlling treatment equipment
<li> Collecting historical data for trending applications
<li> Monitoring equipment
</p>
</details>

* Maintains and repairs electrical breakers and relays, pneumatic-operated valves, electrical tools, etc.

* Maintains and repairs safety equipment.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Gas analyzers
<li> Respirators
<li> Hearing protection equipment
<li> Other personal protective equipment
</p>
</details>

* Calibrates and repairs equipment associated with chemical feed systems.
<details><summary>This includes, but is not limited to, the following systems:</summary>
<p>
<li> Sodium hypochlorite
<li> Ammonium hydroxide
<li> Coagulant
<li> Veligon TL-M
<li> Fluoride
<li> Polymer
<li> Phosphoric acid
</p>
</details>

* Installs and maintains electronic devices and equipment.
<details><summary>Includes, but is not limited to:</summary>
<p>
<li> Programmable controllers
<li> Chemical analyzers
<li> Remote terminal units
<li> Uninterruptible power supply systems
<li> Modem and other telecommunication devices
<li> Video equipment
<li> Circuit boards for supervisory, control, and data acquisition systems
<li> Low voltage communication wire
<li> Plant security system
</p>
</details>

* Develops and executes plans for fixing problems.

* Responds to emergencies.

* Performs preventive and breakdown maintenance as needed.

* Repairs and maintains heating and ventilation (HVAC) systems.

* Repairs, installs, and replaces plumbing as needed.

* Repairs, installs, and replaces  electrical conduit as needed.

* Installs and repairs equipment pertaining to treatment basins, rapid sand filters, UV disinfection and other treatment processes.

* Installs new equipment as needed.

* Replaces and upgrades worn or broken equipment.

* Replaces light bulbs, light-emitting diodes, and other illumination devices in control panel and other plant areas.

* Performs building and grounds functions, including grass cutting and snow removal.

**Operations Duties - 15% of Time**

* Maintains correct water levels at all stages of the water treatment process.

* Maintains correct pressure at all stages of water treatment process.

* Starts and stops process equipment.

* Makes operational and security rounds at frequency and time determined by management.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Walking throughout the entire building
<li> Pushing buttons at designated stations
<li> Making visual observations of equipment and facilities
<li> Listening to equipment and other background noise
<li> Using other senses and observation techniques as needed
<li> Perform routine water quality analyses required during each round
</p>
</details>

* Operates and maintains filters.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Draining
<li> Washing
<li> Refilling
<li> Operating via supervisory control and data acquisition (SCADA) system.
<li> Operating via local controls
<li> Modifying operating parameters
<li> Performing filter drop test
</p>
</details>

* Operates, adjusts, and calibrates chemical feed systems.
<details><summary>The systems include, but are not limited to:</summary>
<p>
<li> Sodium hypochlorite
<li> Aqua ammonia
<li> Coagulant
<li> Veligon TL-M
<li> Fluoride
<li> Polymer
<li> Phosphoric Acid
</p>
</details>

* Operates all systems in plant using manual/local controls, as well as automatic/remote controls.

* Performs public relations duties. This includes, but is not limited to:
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Answering the telephone in a polite and courteous manner
<li> Greeting visitors at door
<li> Collecting necessary information and materials from clients requesting water testing.
</p>
</details>

* Performs laboratory tests.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Chlorine residual (free and total)
<li> Free ammonia residual
<li> Nitrite analysis
<li> Nitrate analysis
<li> Fluoride residual
<li> Phosphate residual
<li> Hardness
<li> Alkalinity
<li> pH
<li> Heterotrophic plate count
<li> Most-Probable-Number (MPN) of Total Coliform and E. Coli.
<li> Presence Absence of Total Coliform and E. Coli.
<li> Odor
<li> Jar test for coagulant
<li> Conductivity
</p>
</details>

* Operates laboratory equipment.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Spectrophotometer
<li> pH meter
<li> Turbidimeter
<li> Autoclave
</p>
</details>

* Mixes and dilutes laboratory chemicals and reagents as needed.

* Maintains accurate records and reports.

* Operates sludge removal system.

* Troubleshoots and responds to operational problems.
<details><summary>Problems include, but are not limited to:</summary>
<p>
<li> Chemical feed malfunction
<li> Erratic pumpage
<li> Erratic performance of filters
<li> Power failure
<li> Equipment failure
</p>
</details>

* Use computers for record-keeping, reporting, and supervisory control of treatment processes.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Using mouse and keyboard to enter data
<li> Changing setpoints and operational parameters
<li> Data entry into spreadsheet, forms or database
<li> Operating word processing, spreadsheet, and database programs
<li> Printing lab results
<li> Completes log sheets.
</p>
</details>

* Understands chemical, mechanical, and hydraulic principles of water treatment processes.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Flocculation
<li> Sedimentation
<li> Sludge removal
<li> Filtration
<li> Disinfection
<li> Filter backwash
<li> Water transfer via pumps
<li> Clearwell storage
<li> Reservoir storage
<li> Washwater storage
<li> Pressurization of distribution system
</p>
</details>

* Understands electrical system layout for Water Treatment Plant and Klode Park Pumping Station.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Transfer switchgear location
<li> Electrical bus configuration
<li> Location of high voltage areas
<li> Location of electrical breakers and fuses
<li> Location of three phase and single phase circuits for process and monitoring equipment
</p>
</details>

* Performs tasks on electrical system.
<details><summary>This includes, but is not limited to:</summary>
<p>
<li> Resetting breakers
<li> Starting and stopping emergency power generators
<li> Operating switchgear
</p>
</details>

**Other Duties - 5% of Time**

* Takes inventory of chemicals and other supplies.
* Assists with emergency breakdowns.
* Assists Relief Operator Technician as needed.
* Assists Operations Supervisor as needed.
* Assists Maintenance Supervisor as needed.
* Assists Plant Manager as needed.
* Regular and predictable attendance, as well as compliance with the Commission’s policies and procedures, is an essential function of the job.


Nothing in this job description limits the Commission’s right to assign or reassign duties and responsibilities to this job at any time. The job description may be changed at any time by the Commission. The duties listed above are intended only as illustrations of the various types of work that may be performed. The omission of specific statements of duties does not exclude them from the position if the work is similar, related or a logical assignment to the position. All job functions must be performed in a manner satisfactory to the Commission.

## REQUIREMENTS OF WORK
* * *

Requires considerable experience in the maintenance and repair of electrical, pneumatic, and computerized equipment. Must have experience and training which provides, with or without reasonable accommodation when applicable, for the following knowledge, abilities, and skills:

* Possess a valid driver's license with good driving record.
* Understanding and following work rules and operating guidelines.
* Ability to read gauges, record data, and take appropriate actions.
* Must be dependable, resourceful, and able to use good judgment.
* Ability to work well with others with a proper attitude toward supervision, fellow workers and the public.
* Ability to follow directions and carry out orders and instructions.
* Ability to read and write documents.
* Ability to perform basic math such as addition, subtraction, multiplication and division.
* Organize, direct and coordinate written and oral reports.
* Effective communication, oral and written, with supervisors, peers, and public.
* Abilities to enter data into a computer, programmable controller, and related equipment.
* Knowledge of confined space entry procedures.
* Considerable knowledge of electrical, pneumatic, and computerized equipment.
* Considerable knowledge of hazards and safety precautions.
* Basic knowledge of electronics.
* Skill in reading plans, blueprints, and wiring diagrams.
* Skill in locating and repairing defects in electrical systems and equipment.
* Ability to communicate effectively with the personnel at various levels within and outside of the organization.
* Ability to maintain and run emergency generating equipment.
* Possess and maintain WDNR waterworks operator certification--subclass surface water (S)--within 6 months of hiring.


## ENVIRONMENTAL/WORKING CONDITIONS OF POSITION:
* * *

The following environmental characteristics described here are representative of those an employee encounters while performing the essential functions of this job. Reasonable accommodations may be made to enable individuals with disabilities to perform essential functions.

* Standing, walking and sitting.
* Climbing, using legs and feet, balancing, bending and twisting.
* Reaching, feeling, talking and hearing.
* Far vision at 20 feet or further and near vision at 20 inches or less, with glasses if needed.
* Stooping, kneeling and crouching.
* Lift and/or move up to 50 lbs.
* Fingering such as to perform wiring, soldering and writing; operate switches; and, work with small parts.
* Standing for at least 30 continuous minutes in laboratory while conducting analyses.
* Ability to work in wet and/or humid conditions.
* Ability to work in outside weather conditions, including extreme cold/heat.
* Physically confined to worksite: cramped, small or restricted making it difficult to stand, sit or walk.
* Ability to work in cold and damp areas for extended periods of time.

## Supervisor
* * *

This position works under the general supervision of the Maintenance Supervisor.

## Interested?
* * *

Please visit and complete the [Interest Form](https://forms.gle/BszMZzvtGaz6k1tp6). If you cannot access the form, please contact Eric Kiefer at info@northshorewc.com or (414) 963-0160. This position may be advertised until filled.


* * *
**Dated 10/22/2020**
